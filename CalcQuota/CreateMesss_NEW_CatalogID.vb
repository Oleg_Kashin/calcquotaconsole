﻿Imports System.Xml.Serialization
Imports System.IO
Imports System.IO.Compression
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient


Public Class CreateMesss_NEW_CatalogID

    ' Public ext As New Extract

    Public LDistr As New List(Of Distributers)
    Public DicCustName As New Dictionary(Of String, String)

    Public LProducts As New List(Of Products)
    Public DProducts As New Dictionary(Of String, Products)

    Public LCatalog As New List(Of Catalog)
    Public DCatalog As New Dictionary(Of String, Catalog)

    Public LREMaster As New List(Of REMaster)
    Public DREMaster As New Dictionary(Of String, REMaster)

    Public LChains As New List(Of Chainmaster)
    Public DChains As New Dictionary(Of String, Chainmaster)

    Public LTTDetails As New List(Of TT)
    Public DTTDetails As New Dictionary(Of String, TT)

    Public LSalesByMonth As New List(Of SalesByMonth)
    Public DSalesByMonth As New SortedDictionary(Of String, List(Of SalesByMonth))

    Public GPath As String '= "D:\Disc D\WORK\XAF\Colgate\In\19112013\"
    Private sp As New SharedProcc
    ' Private pathF As String

    Public Sub Startt(ByVal p As String) ' , ByRef p1 As String)

        GPath = p

        loadDistr()
        LoadProducts()
        LoadCatalog()
        LoadTTDetails()

        LoadREMaster()
        LoadChains()

        LoadSales()

        Console.WriteLine("Befor CreateXML " & Now)

        CreateXML()

        ' p1 = My.Settings.PathFact0

    End Sub

#Region "Load"

    Public Sub loadDistr()

        Dim sqlConn As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK As New SqlClient.SqlCommand()
        sqlComK.CommandType = CommandType.Text
        sqlComK.Connection = sqlConn
        sqlComK.CommandTimeout = 6000

        Try
            sqlConn.Open()
            sqlComK.CommandText = String.Format("SELECT * FROM [dbo].[Distr]")

            Dim rw As SqlDataReader = sqlComK.ExecuteReader
            While rw.Read
                Dim cd As New Distributers
                cd.DistCode = rw.Item("DistCode").ToString
                Try
                    cd.NameDistributor = rw.Item("NameDistributor")
                Catch ex As Exception
                    Console.WriteLine(cd.NameDistributor.ToString & " " & ex.Message)
                End Try
                cd.CustomerNamelegal = rw.Item("CustomerNamelegal").ToString
                cd.AddressLegal = rw.Item("AddressLegal").ToString
                cd.City = rw.Item("City").ToString
                cd.INN = rw.Item("INN").ToString
                cd.KPP = rw.Item("KPP").ToString
                LDistr.Add(cd)
            End While
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        For Each r In LDistr
            If Not DicCustName.ContainsKey(r.DistCode) Then
                DicCustName.Add(r.DistCode, r.NameDistributor)
            End If
        Next

    End Sub

    Public Sub LoadProducts()

        '   ImportTxtProducts(GPath & "ProductHierarchy.txt")
        ImportTxtProducts(GPath & "ProductHierarchyWithIndex.txt")

        DProducts.Clear()
        For Each r In LProducts ' .Where(Function(f) f.net = "LENTA") ' "ASHAN")
            If Not DProducts.ContainsKey(r.SkuID) Then
                DProducts.Add(r.SkuID, r)
            End If
        Next
    End Sub
    Public Sub ImportTxtProducts(ByVal f As String)
        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            Dim l As String
            While Not MyReader.EndOfData
                Try
                    l = MyReader.ReadLine
                    currentRow = l.Split(vbTab)
                    If h > 1 Then
                        AddProducts(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddProducts(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddProducts(ByVal Arr() As String)

        Try
            Dim cu As New Products

            cu.SkuID = Arr(0)
            cu.ManfCode = Arr(1).Replace("'", "''")
            cu.BarCode = Arr(2).Replace("'", "''")
            cu.TextCode = Arr(3).Replace("'", "''")
            cu.SkuName = Arr(4).Replace("'", "''")

            cu.MustSkuName = Arr(5).Replace("'", "''")
            cu.ItemsPerCase = Arr(6).Replace("'", "''")
            cu.SizeBI6levelhierarchy = Arr(7).Replace("'", "''")
            cu.CurrentPrice = Arr(8).Replace(".", ",") ' .Replace("'", "''")
            cu.NewVariantCode = Arr(9).Replace("'", "''")
            cu.NewVariantBI6levelhierarchy = Arr(10).Replace("'", "''")
            cu.NewVariantBI6levelhierarchyRUS = Arr(11).Replace("'", "''")
            cu.NewSubBrandCode = Arr(12).Replace("'", "''")
            cu.NewSubBrandBI6levelhierarchy = Arr(13).Replace("'", "''")
            cu.NewSubBrandBI6levelhierarchyRUS = Arr(14).Replace("'", "''")
            cu.ECPPH3code = Arr(15).Replace("'", "''")
            cu.HierarchyEC = Arr(16).Replace("'", "''")
            cu.SubCategoryCode = Arr(17).Replace("'", "''")
            cu.SubCategory = Arr(18).Replace("'", "''")
            cu.SubCategoryRUS = Arr(19).Replace("'", "''")
            cu.CatogoryCode = Arr(20).Replace("'", "''")
            cu.Categoryname = Arr(21).Replace("'", "''")

            LProducts.Add(cu)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Console.ReadKey()

        End Try


    End Sub

    Public Sub LoadCatalog()

        ImportTxtCatalog(GPath & "Catalog.txt")
        'Try
        '    Dim serializer As New XmlSerializer(GetType(List(Of Catalog)))
        '    Using fs As New FileStream("Catalog.xml", FileMode.Open)
        '        Using w1 As New StreamReader(fs)
        '            LCatalog = serializer.Deserialize(w1)
        '        End Using
        '    End Using
        'Catch ex As Exception
        '    Console.WriteLine("Err LCatalog Read: " & ex.Message)
        'End Try

        DCatalog.Clear()
        For Each r In LCatalog
            If Not DCatalog.ContainsKey(r.CatalogID) Then
                DCatalog.Add(r.CatalogID, r)
            End If
        Next
    End Sub

    Public Sub ImportTxtCatalog(ByVal f As String)

        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    If h > 1 Then
                        AddCatalog(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddCatalog(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddCatalog(ByVal Arr() As String)

        Dim cu As New Catalog
        cu.CatalogID = Arr(0)
        cu.SkuID = Arr(1).Replace("'", "''")
        cu.CISLink_Disabled = Arr(2).Replace("'", "''")

        LCatalog.Add(cu)

    End Sub


    Public Sub LoadREMaster()

        ImportTxtREMaster(GPath & "REMaster.txt")

        DREMaster.Clear()
        For Each r In LREMaster
            If Not DREMaster.ContainsKey(r.RECode) Then
                DREMaster.Add(r.RECode, r)
            End If
        Next
    End Sub
    Public Sub ImportTxtREMaster(ByVal f As String)

        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            Dim l As String
            While Not MyReader.EndOfData
                Try
                    l = MyReader.ReadLine
                    currentRow = l.Split(vbTab)
                    If h > 1 Then
                        AddREMaster(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddREMaster(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddREMaster(ByVal Arr() As String)

        Dim cu As New REMaster

        cu.GroupRECode = Arr(0)
        cu.GroupREName = Arr(1).Replace("'", "''")
        cu.GroupRENameRus = Arr(2).Replace("'", "''")
        cu.RECode = Arr(3).Replace("'", "''")
        cu.REName = Arr(4).Replace("'", "''")
        cu.RENameRus = Arr(5).Replace("'", "''")

        LREMaster.Add(cu)

    End Sub

    Public Sub LoadChains()

        ImportTxtChainmaster(GPath & "Chainmaster.txt")

        DChains.Clear()
        For Each r In LChains
            If Not DChains.ContainsKey(r.ChID) Then
                DChains.Add(r.ChID, r)
            End If
        Next
    End Sub
    Public Sub ImportTxtChainmaster(ByVal f As String)

        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            Dim l As String
            While Not MyReader.EndOfData
                Try
                    l = MyReader.ReadLine
                    currentRow = l.Split(vbTab)
                    If h > 1 Then
                        AddChainmaster(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Console.WriteLine("Line " & ex.Message & "is not valid and will be skipped.")
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddChainmaster(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddChainmaster(ByVal Arr() As String)
        Dim cu As New Chainmaster
        cu.ChID = Arr(0)
        cu.ChainName = Arr(1).Replace("'", "''")
        cu.ChainNameRuss = Arr(2).Replace("'", "''")
        cu.ChainType = Arr(3)
        cu.ChainTypeID = Arr(4)

        LChains.Add(cu)
    End Sub

    Public Sub LoadTTDetails()

        ImportTxtTTDetails(GPath & "TTDetails.txt")

        DTTDetails.Clear()
        For Each r In LTTDetails
            If Not DTTDetails.ContainsKey(r.ContractorSAPCode) Then
                DTTDetails.Add(r.ContractorSAPCode, r)
            End If
        Next

        ObrTTDetails()
    End Sub
    Public Sub ImportTxtTTDetails(ByVal f As String)
        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)

            Dim currentRow As String()
            Dim l As String
            While Not MyReader.EndOfData
                Try
                    l = MyReader.ReadLine
                    currentRow = l.Split(vbTab)
                    If h > 1 Then
                        AddTTDetails(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddTTDetails(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddTTDetails(ByVal Arr() As String)

        Dim cu As New TT
        cu.ContractorSAPCode = Arr(0)
        ' cu.DistrCode = Arr(1).Replace("'", "''")
        ' cu.City = Arr(2).Replace("'", "''")
        cu.TTNameRuss = Arr(3).Replace("'", "''")
        cu.Address0 = Arr(4).Replace("'", "''")
        Try
            cu.SpoAddress = Arr(5).Replace("'", "''")
        Catch ex As Exception
            cu.SpoAddress = ""
            'Console.WriteLine(ex.Message)
        End Try
        ' cu.MCID = Arr(5).Replace("'", "''")
        ' cu.ChID = Arr(6).Replace("'", "''")

        LTTDetails.Add(cu)
    End Sub
    Public Sub ObrTTDetails()

        Dim DicAddr As New Dictionary(Of String, Integer)
        Dim DicSpoAddr As New Dictionary(Of String, Integer)

        For Each r In LTTDetails

            If r.Address0 <> "" Then
                If Not DicAddr.ContainsKey(r.Address0) Then
                    DicAddr.Add(r.Address0, 1)
                Else
                    DicAddr.Item(r.Address0) += 1
                End If
            End If

            If r.SpoAddress <> "" Then
                If Not DicSpoAddr.ContainsKey(r.SpoAddress) Then
                    DicSpoAddr.Add(r.SpoAddress, 1)
                Else
                    DicSpoAddr.Item(r.SpoAddress) += 1
                End If
            End If

        Next

        For Each r In LTTDetails
            Try
                If DicSpoAddr.ContainsKey(r.SpoAddress) And DicAddr.ContainsKey(r.Address0) Then
                    If DicSpoAddr.Item(r.SpoAddress) <= DicAddr.Item(r.Address0) Then
                        r.Address = r.SpoAddress
                    Else
                        r.Address = r.Address0
                    End If
                Else
                    If DicSpoAddr.ContainsKey(r.SpoAddress) Then
                        r.Address = r.SpoAddress
                    End If
                    If DicAddr.ContainsKey(r.Address0) Then
                        r.Address = r.Address0
                    End If
                End If

            Catch ex As Exception

            End Try
        Next

    End Sub

    Public Sub LoadSales()
        ImportTxtSalesByMonth(GPath & "SalesByMonth.txt")

        DSalesByMonth.Clear()
        For Each r In LSalesByMonth '.Where(Function(f) f.DistrCode = "RU00000006")
            Try
                Dim l As New SalesByMonth
                l.DistrCode = r.DistrCode
                l.ChID = r.ChID
                l.ContractorSAPCode = r.ContractorSAPCode
                l.MCID = r.MCID
                l.SRID = r.SRID
                l.CatalogID = r.CatalogID
                l.y = r.y
                l.m = r.m
                l.d = r.d
                l.Quantity = r.Quantity
                l.Amount = r.Amount      ' Сумма отгрузок в ценах Колгейт в динамическом прайс листе
                l.AmountDP = r.AmountDP  ' Сумма отгрузок в ценах дистрибутора
                If Not DSalesByMonth.ContainsKey(r.DistrCode) Then
                    Dim ll As New List(Of SalesByMonth)
                    ll.Add(l)
                    DSalesByMonth.Add(r.DistrCode, ll)
                Else
                    DSalesByMonth.Item(r.DistrCode).Add(l)
                End If
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

        Next

    End Sub
    Public Sub ImportTxtSalesByMonth(ByVal f As String)

        Dim h As Integer = 1
        LSalesByMonth.Clear()

        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            Dim l As String
            While Not MyReader.EndOfData
                Try
                    l = MyReader.ReadLine
                    currentRow = l.Split(vbTab)
                    If h > 1 Then
                        AddSalesByMonth(currentRow)
                    End If

                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Console.WriteLine("Line " & ex.Message & "is not valid and will be skipped.")
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddSalesByMonth(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddSalesByMonth(ByVal Arr() As String)

        Dim cu As New SalesByMonth

        cu.DistrCode = Arr(0)
        cu.ContractorSAPCode = Arr(1).Replace("'", "''")
        cu.SRID = Arr(2).Replace("'", "''")
        cu.MCID = Arr(3).Replace("'", "''")
        Try
            cu.ChID = Arr(4).Replace("'", "''")
        Catch ex As Exception
            cu.ChID = ""
        End Try
        cu.CatalogID = Arr(5).Replace("'", "''")
        cu.Quantity = Arr(6) '.Replace("'", "''")
        cu.AmountDP = Arr(7) ' .Replace("'", "''")
        cu.Amount = Arr(8) ' .Replace("'", "''")
        cu.y = Arr(9) ' .Replace("'", "''")
        cu.m = Arr(10) ' .Replace("'", "''")
        Try
            cu.d = Arr(11)
        Catch ex As Exception
            cu.d = 1
        End Try
        LSalesByMonth.Add(cu)

    End Sub

#End Region

    Public Sub CreateXML()

        Dim Y, M As Integer


        Dim i As Integer = 1
        Y = DSalesByMonth.First.Value.First.y
        M = DSalesByMonth.First.Value.First.m

        For Each rd As KeyValuePair(Of String, List(Of SalesByMonth)) In DSalesByMonth

            Dim md As New MesssageData
            md.DateStart = New Date(Y, M, 1)
            md.DateEnd = New Date(Y, M, Date.DaysInMonth(Y, M))
            If DicCustName.ContainsKey(rd.Key) Then
                md.UrL = DicCustName.Item(rd.Key) '  Название
            Else
                md.UrL = rd.Key '  Название
            End If
            md.Sklad = rd.Key ' Код

            Dim dicSKU As New Dictionary(Of String, String)
            Dim dicCust As New Dictionary(Of String, String)

            Dim o, n, c, tn, cdt, area, CID, SID As String
            Dim Regex As Regex = New Regex("\s")

            For Each r In rd.Value
                Try

                    CID = r.CatalogID


                    If CID <> "0" Then


                        If DCatalog.ContainsKey(r.CatalogID) Then
                            SID = DCatalog.Item(r.CatalogID).SkuID
                        Else
                            ' Console.WriteLine("Bad CatalogID " & r.CatalogID)
                            SID = r.CatalogID
                        End If

                        '  If DItems.ContainsKey(DCatalog.Item(r.CatalogID).SkuID) Then

                        Dim isk As New ItemSKU
                        Dim ic As New ItemCust
                        Dim ina As New ItemNakl
                        o = ""
                        n = ""
                        c = ""
                        tn = "ПУСТО"
                        cdt = ""
                        area = ""

                        Try
                            ' item

                            If Not dicSKU.ContainsKey(CID) Then
                                isk.SKUY = True ' Признак сегментации True -да
                                '  isk.SKULoc = r.Key.art
                                isk.SKULoc = CID '  DItems.Item(DCatalog.Item(r.CatalogID).SkuID).TextCode
                                isk.NameSKU = CID
                                isk.PropSKU20 = SID

                                If DProducts.ContainsKey(SID) Then
                                    isk.NameSKU = DProducts.Item(SID).SkuName
                                    isk.PropSKU18 = DProducts.Item(SID).BarCode    ' globla code
                                    isk.PropSKU19 = DProducts.Item(SID).TextCode ' global name

                                    isk.Categ = DProducts.Item(SID).Categoryname  '  Local Category
                                    isk.PropSKU15 = DProducts.Item(SID).CatogoryCode
                                    isk.NewCateg = DProducts.Item(SID).SubCategory  '  Local Sub-Category
                                    isk.PropSKU16 = DProducts.Item(SID).SubCategoryCode
                                    isk.NewSegment = DProducts.Item(SID).HierarchyEC  '  New Segment
                                    isk.PropSKU17 = DProducts.Item(SID).ECPPH3code
                                    isk.NewGruppa = DProducts.Item(SID).NewVariantBI6levelhierarchy '  New Variant
                                    isk.NewSubBrend = DProducts.Item(SID).NewSubBrandBI6levelhierarchy          '  New Sub Brand
                                    isk.Size01 = DProducts.Item(SID).SizeBI6levelhierarchy  '  Size
                                    isk.EdIzm = DProducts.Item(SID).ItemsPerCase   '  Size Name
                                Else
                                    Console.WriteLine("No DItems " & SID)
                                End If


                                dicSKU.Add(CID, "1")
                                md.ListItem.Add(isk)
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Item " & ex.Message)
                        End Try

                        'Console.WriteLine(r.Key.store)

                        Try
                            ' cust

                            If Not dicCust.ContainsKey(r.ChID & vbTab & r.ContractorSAPCode) Then
                                ic.CustY = True
                                ic.CustIDLoc = r.ChID & vbTab & r.ContractorSAPCode 'r.ContractorSAPCode
                                ic.CustName = r.ContractorSAPCode

                                If DTTDetails.ContainsKey(r.ContractorSAPCode) Then
                                    ic.CustName = DTTDetails.Item(r.ContractorSAPCode).TTNameRuss
                                    ic.AdrName = DTTDetails.Item(r.ContractorSAPCode).Address
                                Else
                                    ic.AdrName = "нет"
                                End If

                                ic.CustKateg21 = r.MCID
                                If DREMaster.ContainsKey(r.MCID) Then
                                    ic.CustKateg1 = DREMaster.Item(r.MCID).GroupREName
                                    ic.CustKateg2 = DREMaster.Item(r.MCID).REName
                                End If

                                ic.CUST3 = r.ChID.ToString
                                If DChains.ContainsKey(r.ChID) Then
                                    ic.CUST1 = DChains.Item(r.ChID).ChainName
                                    ic.CUST2 = DChains.Item(r.ChID).ChainType
                                End If

                                If ic.AdrName Is Nothing Then
                                    ic.AdrName = ""
                                End If

                                If ic.AdrName <> "нет" And ic.AdrName <> "" Then
                                    Try
                                        '  ext.Ext(ic.AdrName, c, o, n, tn)
                                    Catch ex1 As Exception
                                        Console.WriteLine(ex1.Message)
                                    End Try
                                End If


                                If o = "" Then
                                    o = "ОКРУГ_ПУСТО"
                                End If
                                If n = "" Then
                                    n = "ОКАТО_ПУСТО"
                                End If
                                If c = "" Then
                                    c = "Город_ПУСТО"
                                End If
                                If tn = "" Then
                                    tn = "ПУСТО"
                                End If

                                ic.okrug = o
                                ic.okato = n
                                ic.city = c
                                ic.typeN = tn

                                ic.INN = 0
                                ic.KPP = 0

                                dicCust.Add(r.ChID & vbTab & r.ContractorSAPCode, "1")
                                md.ListCust.Add(ic)
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Cust " & ex.Message)
                        End Try

                        Try
                            ' nakl
                            ina.DateNakl = New Date(r.y, r.m, r.d)
                            ina.CustIDLoc = r.ChID & vbTab & r.ContractorSAPCode ' r.ContractorSAPCode
                            ina.SKULoc = CID 'DItems.Item(DCatalog.Item(r.CatalogID).SkuID).TextCode
                            ina.Koeff = 1
                            ina.Kolvo = r.Quantity.Replace(".", ",")
                            ina.Summa = r.AmountDP.Replace(".", ",") ' w/o NDS
                            ina.SummaNet = r.Amount.Replace(".", ",") ' with NDS 
                            ina.SummaCost = 0 ' Cost w/o NDS

                            md.ListNakl.Add(ina)
                        Catch ex As Exception
                            Console.WriteLine("Nakl " & ex.Message)
                        End Try

                    End If

                Catch ex As Exception
                    Console.WriteLine(ex.Message)
                End Try
            Next

            ' save
            Dim mo As String
            Dim io As String
            If M < 10 Then
                mo = "0" & M
            Else
                mo = M
            End If

            'If i < 10 Then
            '    io = "0" & i
            'Else
            '    io = i
            'End If
            io = rd.Key.ToString

            Try

                System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory & "\SalesCatalogID\" & Y & "_" & M)


                My.Settings.PathFact0 = System.IO.Directory.GetCurrentDirectory & "\SalesCatalogID\" & Y & "_" & M

                Dim serializer As New XmlSerializer(GetType(MesssageData))
                Using fs As New FileStream(My.Settings.PathFact0 & "\S_" & Y & "_" & mo & "_1_" & io.ToString & "_01.zip", FileMode.Create)
                    Using ZipOutTree As New DeflateStream(fs, CompressionMode.Compress)
                        Using w1 As New StreamWriter(ZipOutTree)
                            serializer.Serialize(w1, md)
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                Console.WriteLine("Err ListDicSegm Writer1: " & ex.Message)
            End Try

            'If io.ToString = "RU00000006" Then
            '    Try
            '        Dim path As String = "D:\Disc D\Visual Studio\Работа\ПРОЕКТЫ\ImportTxt_ColgatePromo_CA\ImportTxt_ColgatePromo_CA\bin\x64\Debug\SalesCatalogID\" & Y & "_" & M

            '        Dim serializer As New XmlSerializer(GetType(MesssageData))
            '        Using fs As New FileStream(Path & "\S_" & Y & "_" & mo & "_1_" & io.ToString & "_01..xml", FileMode.Create)
            '            Using w1 As New StreamWriter(fs)
            '                serializer.Serialize(w1, md)
            '            End Using
            '        End Using
            '    Catch ex As Exception
            '        Console.WriteLine("Err ListDicSegm Writer1: " & ex.Message)
            '    End Try
            'End If

            i += 1


        Next

    End Sub

End Class

