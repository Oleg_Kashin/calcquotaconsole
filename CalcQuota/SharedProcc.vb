﻿Imports System.Xml.Serialization
Imports System.IO
Imports System.IO.Compression
Imports System.Data.SqlClient
Imports System.Globalization

Public Class SharedProcc

    Public dicInvT As New Dictionary(Of String, String)
    Public DIT As Dictionary(Of String, String)
    Public dicTT As New Dictionary(Of String, String)
    Public dicTTDSF As New Dictionary(Of String, String)
    Public dicNationalProgramSKU As New Dictionary(Of String, List(Of String))
    Private dicNPSKU As New Dictionary(Of String, String)
    Private DEventsCatalog As New Dictionary(Of String, EventsCatalog)
    Public LEventsCatalog As New List(Of EventsCatalog)
    Public y, m As Integer
    Public DDistr As New Dictionary(Of String, Distributers)
    Private dicSellInTxt As New Dictionary(Of String, DistrSellIn)
    Public DicSI As New Dictionary(Of String, List(Of DistrSellIn))
    Public DicSCSI As New Dictionary(Of String, List(Of DistrSellIn))
    Public Path As String = ""
    Public PathF As String = ""
    Public LSAP As New List(Of DistrSAPCode)
    Public DicSubCateg As New Dictionary(Of String, String)
    Public DicProduct As New Dictionary(Of String, String)
    '  Public ListSubCateg As New List(Of String)
    Public DicSubCategR As New Dictionary(Of String, String)
    Public DicSubCategN As New Dictionary(Of String, String)
    Public SpravDate As Date

    Public Shared Sub TestWrite(ByVal s1 As String, Optional ByVal b1 As Boolean = True)
        If Not System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory & "\Logs") Then
            System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory & "\Logs")
        End If

        Using fs As New StreamWriter("Logs\JournalError_" & Now.Day & "." & Now.Month & "." & Now.Year & ".txt", b1)
            fs.WriteLine(s1 & "::: " & DateTime.Now.ToLongDateString & " " & DateTime.Now.ToLongTimeString)
        End Using
    End Sub

#Region "Import ImportTxtDistrSAPCodesChanges"
    Public Sub ImportTxtDistrSAPCodes(ByVal f As String)
        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    If h > 1 Then
                        AddEvents(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddEvents(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddEvents(ByVal Arr() As String)
        Dim d As Date

        Dim cu As New DistrSAPCode

        cu.CodeOLD = Arr(0)
        cu.CodeNEW = Arr(1).Replace("'", "''")
        Try
            d = Arr(2).Replace("'", "''")
            cu.Y = d.Year
            cu.M = d.Month

            LSAP.Add(cu)

        Catch ex As Exception
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row166  " & ex.Message)

        End Try

    End Sub

#End Region

#Region "InvType"

    Public Sub ImportTxtInvType(ByVal f As String)


        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    If h > 1 Then
                        AddInvType(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddInvType(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddInvType(ByVal Arr() As String)
        If Not dicInvT.ContainsKey(Arr(0)) Then
            dicInvT.Add(Arr(0), Arr(1).Replace("'", "''"))
        End If
    End Sub

#End Region

#Region "TTDetails"
    Public Sub ImportTxtTTDetails(ByVal f As String)

        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)

            Dim currentRow As String()
            Dim l As String
            While Not MyReader.EndOfData
                Try
                    l = MyReader.ReadLine
                    currentRow = l.Split(vbTab)
                    If h > 1 Then
                        AddTTDetails(currentRow)
                    End If
                    h += 1
                    'currentRow = MyReader.ReadFields()
                    'If h > 1 Then
                    '    AddTTDetails(currentRow)
                    'End If
                    'h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddTTDetails(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub

    Private Sub AddTTDetails(ByVal Arr() As String)

        Try
            If Not dicTT.ContainsKey(Arr(0).ToString.Replace("'", "''")) Then
                Try
                    If dicInvT.ContainsKey(Arr(10).Replace("'", "''")) Then
                        dicTT.Add(Arr(0).ToString.Replace("'", "''"), dicInvT.Item(Arr(10).Replace("'", "''")))
                    Else
                        dicTT.Add(Arr(0).ToString.Replace("'", "''"), "blank")
                    End If

                Catch ex As Exception

                End Try

            End If

            If Not dicTTDSF.ContainsKey(Arr(0).ToString.Replace("'", "''")) Then
                Try
                    Select Case Arr(8)
                        Case 1520, 1519, 1518
                            dicTTDSF.Add(Arr(0).ToString.Replace("'", "''"), "NoDSF")
                        Case Else
                            dicTTDSF.Add(Arr(0).ToString.Replace("'", "''"), "DSF")
                    End Select
                Catch ex As Exception

                End Try

            End If



        Catch ex As Exception

        End Try

    End Sub
#End Region

#Region "NationalProgramSKU"
    Public Sub ImportTxtNationalProgramSKU(ByVal f As String)
        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    If h > 1 Then
                        AddNationalProgramSKU(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddNationalProgramSKU(s)
                    End If
                    h += 1
                End Try
            End While
        End Using
    End Sub
    Public Sub AddNationalProgramSKU(ByVal Arr() As String)

        If Not dicNationalProgramSKU.ContainsKey(Arr(0)) Then
            Dim l As New List(Of String)
            l.Add(Arr(1).Replace("'", "''"))
            dicNationalProgramSKU.Add(Arr(0), l)
        Else
            dicNationalProgramSKU.Item(Arr(0)).Add(Arr(1).Replace("'", "''"))
        End If

    End Sub

#End Region

#Region "SellIn"

    Public Sub ListNatSKU()

        For Each r In LEventsCatalog
            If dicNationalProgramSKU.ContainsKey(r.EventID) Then
                For Each it In dicNationalProgramSKU.Item(r.EventID)
                    If Not dicNPSKU.ContainsKey(it) Then
                        dicNPSKU.Add(it, "")
                    End If
                Next
            End If
        Next
    End Sub

    Public Sub ImportTxtSellIn(ByVal f As String)

        ListNatSKU()

        Dim h As Integer = 1
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(f, System.Text.Encoding.Default) ' System.Text.Encoding.Default так как кодировка ANSI

            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(vbTab)
            Dim currentRow As String()
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    If h > 1 Then
                        AddSellIn(currentRow)
                    End If
                    h += 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Dim s() As String = MyReader.ErrorLine.Split(vbTab)
                    If h > 1 Then
                        AddSellIn(s)
                    End If
                    h += 1
                End Try
            End While
        End Using

        ListSellIn()

    End Sub
    Public Sub AddSellIn(ByVal Arr() As String)
        Dim d As Date

        Dim cu As New DistrSellIn

        cu.ItemCode = Arr(0).Replace("'", "''").Trim
        cu.Dist = Arr(1).Replace("'", "''").Trim

        If dicNPSKU.ContainsKey(cu.ItemCode) Then
            cu.National = True
        Else
            cu.National = False
        End If

        If DicSubCateg.ContainsKey(cu.ItemCode) Then
            cu.SubCategID = DicSubCateg.Item(cu.ItemCode)
        Else
            cu.SubCategID = "NoSubCateg"
        End If
        cu.Amount = Arr(2).Replace(".", ",")

        'If cu.Item = "253166" Then
        '    Dim asdf As String = "sdfsd"
        'End If
        Try
            d = Arr(3).Replace("'", "''")
            cu.Y = d.Year
            cu.M = d.Month

            If d.Year > 2014 And cu.Amount > 0 Then
                ' If cu.Item = "253166" And cu.Dist = "RU00000006" Then

                If Not dicSellInTxt.ContainsKey(cu.ItemCode & vbTab & cu.Dist & vbTab & cu.Y & vbTab & cu.M) Then
                    dicSellInTxt.Add(cu.ItemCode & vbTab & cu.Dist & vbTab & cu.Y & vbTab & cu.M, cu)
                Else
                    dicSellInTxt.Item(cu.ItemCode & vbTab & cu.Dist & vbTab & cu.Y & vbTab & cu.M).Amount += cu.Amount
                End If
                ' End If
            End If
        Catch ex As Exception
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row166  " & ex.Message)

        End Try

    End Sub

    Private Sub ListSellIn()


        For Each r In dicSellInTxt
            'If r.Value.Dist = "RU00000006" And r.Value.Item = "FTR22270" Then
            '    Dim ddd As String = "sdfsd"
            'End If
            If Not DicSI.ContainsKey(r.Value.Dist) Then
                Dim l As New List(Of DistrSellIn)
                l.Add(r.Value)
                DicSI.Add(r.Value.Dist, l)
            Else
                DicSI.Item(r.Value.Dist).Add(r.Value)
            End If
        Next


    End Sub

#End Region

    Public Sub loadDistr()
        '
        Dim sqlConn0 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        'Dim sqlComK0 As New SqlClient.SqlCommand()
        'sqlComK0.CommandType = CommandType.Text
        'sqlComK0.Connection = sqlConn0
        'sqlComK0.CommandTimeout = 6000

        Try
            sqlConn0.Open()

            'sqlComK0.CommandText = String.Format("SELECT [Oid] Oid " _
            '     & ", [DistCode] DistCode " _
            '     & ", [NameDistributor] as NameDistributor " _
            '        & "FROM [dbo].[Distr] " _
            '        & " where [GCRecord] is null and [IsActiv] = 1 ")

            Dim YY As New SqlParameter("@Y", SqlDbType.Int)
            Dim MM As New SqlParameter("@M", SqlDbType.Int)

            YY.Value = y
            MM.Value = m - 1

            Dim cmd As New SqlCommand(" SELECT [Oid] Oid " _
                                & " , [DistCode] DistCode " _
                                & " , [NameDistributor] as NameDistributor " _
                                & "	, (Select dt.[Target] " _
                                & "		FROM [dbo].[DistrTargets] dt " _
                                & "		where dt.[GCRecord] is null and [YearDT] = @Y and [MonthDT] = @M " _
                                & "		and dt.[TargetTypeID] = 6 and dt.[Distr] = d.[Oid] " _
                                & "	) as Target " _
                                & " FROM [dbo].[Distr] d " _
                                & " where d.[GCRecord] is null and d.[IsActiv] = 1", sqlConn0)

            cmd.Parameters.Add(YY)
            cmd.Parameters.Add(MM)

            Dim rdr As SqlDataReader = cmd.ExecuteReader

            While rdr.Read

                Dim cu As New Distributers
                cu.DistCode = rdr.Item("DistCode")
                cu.DistOid = rdr.Item("Oid").ToString
                cu.NameDistributor = rdr.Item("NameDistributor").ToString
                Try
                    cu.Target = rdr.Item("Target")
                Catch ex As Exception
                    cu.Target = 0
                End Try

                If Not DDistr.ContainsKey(cu.DistCode) And cu.DistCode <> "" Then
                    DDistr.Add(cu.DistCode, cu)
                End If
            End While

            rdr.Close()
        Catch ex As System.Data.SqlClient.SqlException
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row68  " & ex.Message)
        Finally
            sqlConn0.Close()
        End Try
    End Sub

    Public Sub loadSubCateg()
        '
        Dim sqlConn0 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK0 As New SqlClient.SqlCommand()
        sqlComK0.CommandType = CommandType.Text
        sqlComK0.Connection = sqlConn0
        sqlComK0.CommandTimeout = 6000
        '  Dim dic As New Dictionary(Of String, String)

        Try
            sqlConn0.Open()

            sqlComK0.CommandText = String.Format("SELECT [Oid] " _
                        & " ,[TextCode]" _
                        & " ,[SkuName]" _
                        & " ,[Category]" _
                        & " ,[SubCategory] as  OidSubCategory " _
                        & "	,(select sc.[SubCategoryCode]   FROM [dbo].[SubCategories] sc " _
                        & "	where sc.[Oid] = [SubCategory]) as SubCategoryCode " _
                        & " ,(select sc.[SubCategoryName]   FROM [dbo].[SubCategories] sc " _
                        & "	where sc.[Oid] = [SubCategory]) as SubCategoryName " _
                        & "	,(select sc.[SubCategoryRUS]   FROM [dbo].[SubCategories] sc " _
                        & "	where sc.[Oid] = [SubCategory]) as SubCategoryRUS    " _
                        & "	,(select sc.[PHDummySKU] FROM [dbo].[SubCategories] sc " _
                        & "	where sc.[Oid] = [SubCategory]) as PHDummySKU " _
                        & "	,(select sc.[PHDummySKUMaterialSettelment]  FROM [dbo].[SubCategories] sc " _
                        & "	where sc.[Oid] = [SubCategory]) as PHDummySKUMaterialSettelment  " _
                        & " FROM [dbo].[Items] " _
                        & " where [GCRecord] is null ")

            Dim rdr As SqlDataReader = sqlComK0.ExecuteReader

            While rdr.Read

                If Not DicSubCateg.ContainsKey(rdr.Item("TextCode").ToString) And rdr.Item("TextCode").ToString <> "" Then
                    DicSubCateg.Add(rdr.Item("TextCode").ToString, rdr.Item("PHDummySKU").ToString)
                End If

                If Not DicProduct.ContainsKey(rdr.Item("TextCode").ToString) And rdr.Item("TextCode").ToString <> "" Then
                    DicProduct.Add(rdr.Item("TextCode").ToString, rdr.Item("SkuName").ToString)
                End If

                If Not DicSubCategR.ContainsKey(rdr.Item("PHDummySKU").ToString) Then
                    DicSubCategR.Add(rdr.Item("PHDummySKU").ToString, rdr.Item("PHDummySKUMaterialSettelment").ToString)
                    DicSubCategN.Add(rdr.Item("PHDummySKU").ToString, rdr.Item("SubCategoryRUS").ToString)
                    ' ListSubCateg.Add(rdr.Item("SubCategoryCode").ToString)
                End If


            End While

            rdr.Close()
        Catch ex As System.Data.SqlClient.SqlException
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row68  " & ex.Message)
        Finally
            sqlConn0.Close()
        End Try

        ' Sap
        Dim DicSKU As New Dictionary(Of String, String)
        For Each ec In LEventsCatalog
            If dicNationalProgramSKU.ContainsKey(ec.EventID) Then
                For Each r In dicNationalProgramSKU.Item(ec.EventID)
                    If Not DicSKU.ContainsKey(r) Then
                        If DicSubCateg.ContainsKey(r) Then
                            DicSKU.Add(r, DicSubCateg.Item(r))
                        Else
                            DicSKU.Add(r, "no found subcateg")
                        End If
                    End If
                Next
            End If
        Next

    End Sub

    Public Sub loadCalcQuotaNP(ByRef b As Boolean, ByRef bRate As Boolean)


        Dim sqlConn0 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK0 As New SqlClient.SqlCommand()
        sqlComK0.CommandType = CommandType.Text
        sqlComK0.Connection = sqlConn0
        sqlComK0.CommandTimeout = 6000


        Dim conn As New SqlConnection(My.Settings.ConnStr)

        Try
            sqlConn0.Open()

            sqlComK0.CommandText = String.Format("SELECT [Year] " _
                                                 & ",[Month] " _
                                                 & ",[Calc] " _
                                                 & ",[CalcSAPRate] " _
                                                 & ",[SpravDate] " _
                                                 & " FROM [dbo].[NPQuotaCalc] " _
                                                 & " where [GCRecord] Is null")
            Dim rdr As SqlDataReader = sqlComK0.ExecuteReader

            While rdr.Read

                y = rdr.Item("Year")
                m = rdr.Item("Month") + 1
                b = rdr.Item("Calc")
                bRate = rdr.Item("CalcSAPRate")
                If Not b And bRate Then
                    SpravDate = rdr.Item("SpravDate")
                Else
                    SpravDate = Nothing
                End If
            End While

            rdr.Close()
        Catch ex As System.Data.SqlClient.SqlException
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row173  " & ex.Message)
        Finally
            sqlConn0.Close()
        End Try

        Try
            ' снять галку
            conn.Open()

            Dim cmd As New SqlCommand("UPDATE [dbo].[NPQuotaCalc] " _
                       & " SET [Calc]  =  0, [CalcSAPRate]  =  0", conn)


            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row411  " & ex.Message)
        Finally
            sqlConn0.Close()
        End Try

    End Sub

    Public Sub loadQuotaNPKoeff(ByRef k As Double)

        k = 0

        Dim sqlConn0 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK0 As New SqlClient.SqlCommand()
        sqlComK0.CommandType = CommandType.Text
        sqlComK0.Connection = sqlConn0
        sqlComK0.CommandTimeout = 6000
        Dim d As New Date(2010, 1, 1)

        Try
            sqlConn0.Open()

            sqlComK0.CommandText = String.Format("SELECT [Year] " _
                                                 & ",[Month] " _
                                                 & ",[Koeff] " _
                                                 & " FROM [dbo].[NPQuota] " _
                                                 & " where [GCRecord] Is null")
            Dim rdr As SqlDataReader = sqlComK0.ExecuteReader

            While rdr.Read

                Try
                    If d < New Date(rdr.Item("Year"), rdr.Item("Month") + 1, 1) Then
                        d = New Date(rdr.Item("Year"), rdr.Item("Month") + 1, 1)
                        k = rdr.Item("Koeff")
                    End If
                Catch ex As Exception

                End Try
            End While

            rdr.Close()
        Catch ex As System.Data.SqlClient.SqlException
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row173  " & ex.Message)
        Finally
            sqlConn0.Close()
        End Try


    End Sub

#Region "Events Catalog"

    Public Sub loadEC()
        Dim LEventsCatalog0 As New List(Of EventsCatalog)

        Dim sqlConn0 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK0 As New SqlClient.SqlCommand()
        sqlComK0.CommandType = CommandType.Text
        sqlComK0.Connection = sqlConn0
        sqlComK0.CommandTimeout = 6000

        Try
            sqlConn0.Open()

            sqlComK0.CommandText = String.Format("SELECT * FROM [dbo].[EventsCatalog]")
            Dim rdr As SqlDataReader = sqlComK0.ExecuteReader

            While rdr.Read

                Dim cu As New EventsCatalog
                cu.Oid = rdr.Item("Oid").ToString
                cu.EventID = rdr.Item("EventID")
                cu.EventName = rdr.Item("EventName")
                cu.EventNameRuss = rdr.Item("EventNameRuss")
                cu.Quantity = rdr.Item("Quantity")
                cu.Bonus = rdr.Item("Bonus")
                cu.EventsType = rdr.Item("EventsType")
                Try
                    cu.DiscountNP = rdr.Item("DiscountNP")
                Catch ex As Exception
                    cu.DiscountNP = 0
                End Try

                Try
                    cu.EndDate = rdr.Item("EndDate")
                Catch ex As Exception
                    cu.EndDate = Nothing
                End Try

                Try
                    cu.StartDate = rdr.Item("StartDate")
                    If cu.StartDate.Year = y And cu.StartDate.Month = m Then
                        LEventsCatalog0.Add(cu)
                    End If
                Catch ex As Exception
                    cu.StartDate = Nothing
                End Try

                ' If cu.EventsType = 109 Then

                ' End if

            End While

            rdr.Close()
        Catch ex As System.Data.SqlClient.SqlException
            Console.WriteLine("SQL ExecuteNonQuery " & ex.Message)
            SharedProcc.TestWrite(Now & " ------------Row83  " & ex.Message)
        Finally
            sqlConn0.Close()
        End Try


        If LEventsCatalog0.Count > 0 Then
            For Each r In LEventsCatalog0
                If Not DEventsCatalog.ContainsKey(r.EventID) Then
                    DEventsCatalog.Add(r.EventID, r)
                    LEventsCatalog.Add(r)
                End If
            Next
        End If
        LEventsCatalog0.Clear()

    End Sub

#End Region

    Public Sub CheckFact(ByVal p As String) ' , ByRef p1 As String)

        Dim dd As New Date(y, m, 1)
        Dim dd1 As New Date
        dd1 = dd.AddMonths(-2) ' 1)

        Dim mm, yy As String
        mm = dd1.Month
        yy = dd1.Year

        If mm.Length = 1 Then
            mm = "0" & mm
        End If

        If My.Computer.FileSystem.GetFiles(My.Settings.PathFact, _
                      FileIO.SearchOption.SearchTopLevelOnly, "S_" & yy & "_" & mm & "_*.zip").Count = 0 Then

            ' calc
            Dim cr As New CreateMesss_NEW_CatalogID
            cr.Startt(p) ' , p1)
        End If

    End Sub


    Public Sub LoadXMLforNP(ByVal p As String, ByVal sd As Date, ByVal d As String,
                                        ByVal dicT As Dictionary(Of String, String),
                                        ByVal dicTDSF As Dictionary(Of String, String), ByRef listSellOut As List(Of ItemNakl))

        Dim y, m As String

        Dim DicSKU As New SortedDictionary(Of String, String)
        Dim b As Boolean = False

        For rw As Integer = 0 To 5
            y = sd.AddMonths(-7 + rw).Year
            m = sd.AddMonths(-7 + rw).Month()
            If m.Length = 1 Then
                m = "0" & m
            End If
            '  Console.WriteLine("p " & p)
            '  Console.WriteLine("PathF " & PathF)
            '  Console.ReadKey()

            If rw = 5 Then
                If My.Computer.FileSystem.GetFiles(p, _
                       FileIO.SearchOption.SearchTopLevelOnly, "S_" & y & "_" & m & "_*.zip").Count = 0 Then
                    b = True
                End If
            End If

            If b Then
                p = My.Settings.PathFact0
            End If

            For Each foundFile As String In My.Computer.FileSystem.GetFiles(p, _
                    FileIO.SearchOption.SearchTopLevelOnly, "S_" & y & "_" & m & "_*" & d & "_*.zip")

                Dim md As New MesssageData
                Try
                    Dim serializer As New XmlSerializer(GetType(MesssageData))
                    Using fs As New FileStream(foundFile, FileMode.Open)
                        Using ZipOutTree As New DeflateStream(fs, CompressionMode.Decompress)
                            md = serializer.Deserialize(ZipOutTree)
                        End Using
                    End Using
                Catch ex As Exception
                    Console.WriteLine("Err ListDicSegm Writer1: " & foundFile & " - - " & ex.Message)
                End Try

                For Each r In md.ListItem
                    If Not DicSKU.ContainsKey(r.SKULoc) Then
                        DicSKU.Add(r.SKULoc, r.PropSKU18)
                    Else
                        If r.PropSKU18 <> "" Then
                            DicSKU.Item(r.SKULoc) = r.PropSKU18
                        End If
                    End If
                Next

                ' summ = md.ListNakl.Sum(Function(ff) ff.SummaNet)

                Dim codeTT As String
                For Each r In md.ListNakl

                    Dim ln As New ItemNakl

                    codeTT = ""
                    Try
                        Dim arr() As String = r.CustIDLoc.Split(vbTab)
                        codeTT = arr(1)
                    Catch ex As Exception

                    End Try

                    ln.CustIDLoc = codeTT
                    ln.AdrDost = "blank"

                    If dicT.ContainsKey(codeTT) Then
                        ln.AdrDost = dicT.Item(codeTT)
                    End If

                    If dicTDSF.ContainsKey(codeTT) Then
                        ln.NomZakaz = dicTDSF.Item(codeTT)
                    End If

                    If DicSKU.ContainsKey(r.SKULoc) Then
                        ln.SKULoc = DicSKU.Item(r.SKULoc)
                    Else
                        ln.SKULoc = r.SKULoc
                    End If

                    ln.DateNakl = r.DateNakl

                    ln.Kolvo = r.Kolvo
                    ln.Summa = r.Summa
                    ln.SummaNet = r.SummaNet

                    listSellOut.Add(ln)

                Next
            Next
        Next

    End Sub

    Public Sub SaveFile0(ByVal n1 As String, ByVal n As String, ByVal nn1 As String, ByVal nn As String)
        Dim conn As New SqlConnection(My.Settings.ConnStr)
        Dim conn1 As New SqlConnection(My.Settings.ConnStr)
        Dim conn11 As New SqlConnection(My.Settings.ConnStr)
        Dim OidF As New System.Guid
        OidF = System.Guid.NewGuid
        Dim OidF1 As New System.Guid
        OidF1 = System.Guid.NewGuid
        Dim infoReader As New System.IO.FileInfo(n1)
        Dim infoReader1 As New System.IO.FileInfo(nn1)

        Dim sqlConn1 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK1 As New SqlClient.SqlCommand()
        sqlComK1.CommandType = CommandType.Text
        sqlComK1.Connection = sqlConn1
        sqlComK1.CommandTimeout = 6000

        Dim bR As Boolean

        Try
            conn.Open()
            conn1.Open()
            conn11.Open()
            sqlConn1.Open()


            ' -----------------------------------

            ' получить EventID & Distr
            sqlComK1.CommandText = String.Format("SELECT top 1 [Oid] FROM [dbo].[NPQuotaCalc]")
            Dim rdr1 As SqlDataReader = sqlComK1.ExecuteReader

            bR = False
            bR = rdr1.HasRows

            ' Call Read before accessing data.
            Dim EvDC As New System.Guid

            System.Guid.NewGuid()


            Try
                While rdr1.Read()
                    EvDC = rdr1.GetGuid(0)
                End While
            Catch ex As Exception
                bR = False
            End Try


            ' Call Close when done reading.
            rdr1.Close()


            Dim p As New SqlParameter("@Content", SqlDbType.VarBinary, -1)

            Dim sr As System.IO.Stream = System.IO.File.Open(n1, IO.FileMode.Open, IO.FileAccess.Read)
            Dim bytes As Byte() = New Byte(sr.Length - 1) {}
            sr.Read(bytes, 0, bytes.Length)

            p.Value = CompressionUtils.Compress(New MemoryStream(DirectCast(bytes, Byte()))).ToArray()

            Dim cmd As New SqlCommand("INSERT INTO [dbo].[FileData] " _
               & " ([Oid] " _
               & " ,[FileName] " _
               & " ,[size] " _
               & " ,[Content]) " _
             & " VALUES " _
               & " ('" & OidF.ToString & "'" _
               & " ,'" & n & "'" _
                & " , " & infoReader.Length _
               & " ,@Content)", conn)

            cmd.Parameters.Add(p)
            cmd.ExecuteNonQuery()

            Dim p1 As New SqlParameter("@Content1", SqlDbType.VarBinary, -1)

            Dim sr1 As System.IO.Stream = System.IO.File.Open(nn1, IO.FileMode.Open, IO.FileAccess.Read)
            Dim bytes1 As Byte() = New Byte(sr1.Length - 1) {}
            sr1.Read(bytes1, 0, bytes1.Length)

            p1.Value = CompressionUtils.Compress(New MemoryStream(DirectCast(bytes1, Byte()))).ToArray()

            Dim cmd11 As New SqlCommand("INSERT INTO [dbo].[FileData] " _
               & " ([Oid] " _
               & " ,[FileName] " _
               & " ,[size] " _
               & " ,[Content]) " _
             & " VALUES " _
               & " ('" & OidF1.ToString & "'" _
               & " ,'" & nn & "'" _
                & " , " & infoReader1.Length _
               & " ,@Content1)", conn11)

            cmd11.Parameters.Add(p1)
            cmd11.ExecuteNonQuery()



            Dim cmd1 As New SqlCommand("INSERT INTO [dbo].[NPQuotaCalcList] " _
                & " ([Oid] " _
                & " ,[NPQuotaCalc] " _
                & " ,[FileSAP] " _
                & " ,[FileSAPLoad] " _
                & " ,[DateCreate] " _
                & " ,[DateCalc]) " _
              & " VALUES " _
                & " ('" & System.Guid.NewGuid.ToString & "'" _
                & " ,'" & EvDC.ToString & "'" _
                & " ,'" & OidF.ToString & "'" _
                & " ,'" & OidF1.ToString & "'" _
                & " , '" & IIf(My.Settings.ServerRu, Now, Now.ToString("M/d/yyyy h:mm tt", CultureInfo.CreateSpecificCulture("en-US"))) & "'" _
                & " , '" & IIf(My.Settings.ServerRu, New Date(y, m, 1), New Date(y, m, 1).ToString("M/d/yyyy h:mm tt", CultureInfo.CreateSpecificCulture("en-US"))) & "')", conn1)


            cmd1.ExecuteNonQuery()


        Catch ex As Exception
            SharedProcc.TestWrite(Now & " ------- " & " save file " & n & " --- " & ex.Message)
        Finally
            conn.Close()
            conn1.Close()
            conn11.Close()
            sqlConn1.Close()
        End Try
        'save file

    End Sub


    Public Sub SaveFile00(ByVal n1 As String, ByVal n As String)
        Dim conn As New SqlConnection(My.Settings.ConnStr)
        Dim conn1 As New SqlConnection(My.Settings.ConnStr)
        Dim OidF As New System.Guid
        OidF = System.Guid.NewGuid
        Dim infoReader As New System.IO.FileInfo(n1)

        Dim sqlConn1 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK1 As New SqlClient.SqlCommand()
        sqlComK1.CommandType = CommandType.Text
        sqlComK1.Connection = sqlConn1
        sqlComK1.CommandTimeout = 6000

        Dim bR As Boolean

        Try
            conn.Open()
            '  conn1.Open()
            sqlConn1.Open()

            ' получить EventID & Distr
            sqlComK1.CommandText = String.Format("SELECT top 1 [File], [Oid] FROM [dbo].[NPQuotaCalc]")
            Dim rdr1 As SqlDataReader = sqlComK1.ExecuteReader

            bR = False
            bR = rdr1.HasRows

            ' Call Read before accessing data.
            Dim EvDC As New System.Guid
            Dim EvDC1 As New System.Guid

            Try
                While rdr1.Read()
                    EvDC1 = rdr1.GetGuid(1)
                    EvDC = rdr1.GetGuid(0)
                End While
            Catch ex As Exception
                bR = False
            End Try


            ' Call Close when done reading.
            rdr1.Close()



            Dim p As New SqlParameter("@Content", SqlDbType.VarBinary, -1)

            Dim sr As System.IO.Stream = System.IO.File.Open(n1, IO.FileMode.Open, IO.FileAccess.Read)
            Dim bytes As Byte() = New Byte(sr.Length - 1) {}
            sr.Read(bytes, 0, bytes.Length)

            p.Value = CompressionUtils.Compress(New MemoryStream(DirectCast(bytes, Byte()))).ToArray()

            If bR Then
                Dim cmd As New SqlCommand("UPDATE [dbo].[FileData] " _
                        & " SET [FileName] =  '" & n & "'" _
                        & " ,[size] = " & infoReader.Length _
                        & " ,[Content] = @Content " _
                        & " WHERE [Oid] = '" & EvDC.ToString & "'", conn)

                cmd.Parameters.Add(p)

                cmd.ExecuteNonQuery()
            Else
                Dim cmd As New SqlCommand("INSERT INTO [dbo].[FileData] " _
                         & " ([Oid] " _
                         & " ,[FileName] " _
                         & " ,[size] " _
                         & " ,[Content]) " _
                       & " VALUES " _
                         & " ('" & OidF.ToString & "'" _
                         & " ,'" & n & "'" _
                          & " , " & infoReader.Length _
                         & " ,@Content)", conn)

                cmd.Parameters.Add(p)

                cmd.ExecuteNonQuery()
                conn1.Open()

                Dim cmd1 As New SqlCommand("UPDATE [dbo].[NPQuotaCalc] " _
                                & "  SET [File] = '" & OidF.ToString & "'" _
                                & " where [Oid] = '" & EvDC1.ToString & "'", conn1)

                cmd1.ExecuteNonQuery()

            End If

        Catch ex As Exception
            SharedProcc.TestWrite(Now & " ------- " & " save file " & n & " --- " & ex.Message)
        Finally
            conn.Close()
            conn1.Close()
            sqlConn1.Close()
        End Try
        'save file

    End Sub

    Public Sub SaveFile(ByVal oid As String, ByVal n1 As String, ByVal n As String)
        Dim conn As New SqlConnection(My.Settings.ConnStr)
        Dim conn1 As New SqlConnection(My.Settings.ConnStr)
        Dim OidF As New System.Guid
        OidF = System.Guid.NewGuid
        Dim infoReader As New System.IO.FileInfo(n1)

        Dim sqlConn1 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK1 As New SqlClient.SqlCommand()
        sqlComK1.CommandType = CommandType.Text
        sqlComK1.Connection = sqlConn1
        sqlComK1.CommandTimeout = 6000

        Dim bR As Boolean

        Try
            conn.Open()
            '  conn1.Open()
            sqlConn1.Open()

            ' получить EventID & Distr
            sqlComK1.CommandText = String.Format("SELECT [FileCalc] FROM [dbo].[EventsCatalog] where [Oid] = '{0}'",
                                                 oid)
            Dim rdr1 As SqlDataReader = sqlComK1.ExecuteReader

            bR = False
            bR = rdr1.HasRows

            ' Call Read before accessing data.
            Dim EvDC As New System.Guid
            Try
                While rdr1.Read()
                    EvDC = rdr1.GetGuid(0)
                End While
            Catch ex As Exception
                bR = False
            End Try


            ' Call Close when done reading.
            rdr1.Close()



            Dim p As New SqlParameter("@Content", SqlDbType.VarBinary, -1)

            Dim sr As System.IO.Stream = System.IO.File.Open(n1, IO.FileMode.Open, IO.FileAccess.Read)
            Dim bytes As Byte() = New Byte(sr.Length - 1) {}
            sr.Read(bytes, 0, bytes.Length)

            p.Value = CompressionUtils.Compress(New MemoryStream(DirectCast(bytes, Byte()))).ToArray()

            If bR Then
                Dim cmd As New SqlCommand("UPDATE [dbo].[FileData] " _
                        & " SET [FileName] =  '" & n & "'" _
                        & " ,[size] = " & infoReader.Length _
                        & " ,[Content] = @Content " _
                        & " WHERE [Oid] = '" & EvDC.ToString & "'", conn)

                cmd.Parameters.Add(p)

                cmd.ExecuteNonQuery()
            Else
                Dim cmd As New SqlCommand("INSERT INTO [dbo].[FileData] " _
                         & " ([Oid] " _
                         & " ,[FileName] " _
                         & " ,[size] " _
                         & " ,[Content]) " _
                       & " VALUES " _
                         & " ('" & OidF.ToString & "'" _
                         & " ,'" & n & "'" _
                          & " , " & infoReader.Length _
                         & " ,@Content)", conn)

                cmd.Parameters.Add(p)

                cmd.ExecuteNonQuery()
                conn1.Open()

                Dim cmd1 As New SqlCommand("UPDATE [dbo].[EventsCatalog] " _
                                & "  SET [FileCalc] = '" & OidF.ToString & "'" _
                                & " where [Oid] = '" & oid.ToString & "'", conn1)

                cmd1.ExecuteNonQuery()

            End If

        Catch ex As Exception
            SharedProcc.TestWrite(Now & " ------- " & " save file " & n & " --- " & ex.Message)
        Finally
            conn.Close()
            conn1.Close()
            sqlConn1.Close()
        End Try
        'save file

    End Sub

    Public Sub SummDistrQuotas(ByVal EventOid As String, lResult As List(Of NPQuotaCalc))

        Dim DicDistr As New Dictionary(Of String, NPQuotaDistr)

        For Each r In lResult ' .Where(Function(ff) ff.DistrCode = "RU00000006")
            If Not DicDistr.ContainsKey(r.EventID & vbTab & r.DistrCode) Then
                Dim npq As New NPQuotaDistr
                npq.EventID = r.EventID
                npq.EventOid = EventOid
                npq.DistrCode = r.DistrCode
                Try
                    npq.distrOid = DDistr.Item(r.DistrCode).DistOid
                Catch ex As Exception
                    npq.distrOid = ""
                End Try
                npq.Quotas = Math.Round(r.Quota, 2)
                npq.MaxBonus = Math.Round(r.Max_Bonus, 2)
                DicDistr.Add(r.EventID & vbTab & r.DistrCode, npq)
            Else
                DicDistr.Item(r.EventID & vbTab & r.DistrCode).Quotas += Math.Round(r.Quota, 2)
                DicDistr.Item(r.EventID & vbTab & r.DistrCode).MaxBonus += Math.Round(r.Max_Bonus, 2)
            End If
        Next

        InsertDistrQuotas(DicDistr)

    End Sub

    Private Sub InsertDistrQuotas(DicDistr As Dictionary(Of String, NPQuotaDistr))

        Dim conn As New SqlConnection(My.Settings.ConnStr)

        Dim sqlConn1 As New SqlClient.SqlConnection(My.Settings.ConnStr)

        Dim sqlComK1 As New SqlClient.SqlCommand()
        sqlComK1.CommandType = CommandType.Text
        sqlComK1.Connection = sqlConn1
        sqlComK1.CommandTimeout = 6000

        Dim bR As Boolean


        For Each r In DicDistr.Values

            Dim OidF As New System.Guid
            OidF = System.Guid.NewGuid

            Try
                sqlConn1.Open()
                conn.Open()

                ' получить EventID & Distr
                sqlComK1.CommandText = String.Format("SELECT [Oid] FROM [dbo].[DistrQuota] where [Distr] = '{0}' and [EventsCatalog] = '{1}'",
                                                     r.distrOid, r.EventOid)
                Dim rdr1 As SqlDataReader = sqlComK1.ExecuteReader

                bR = False
                bR = rdr1.HasRows

                ' Call Read before accessing data.
                Dim EvDC As New System.Guid
                While rdr1.Read()
                    EvDC = rdr1.GetGuid(0)
                End While

                ' Call Close when done reading.
                rdr1.Close()

                If bR Then
                    Dim cmd As New SqlCommand("UPDATE [dbo].[DistrQuota] " _
                        & "SET [SumOfQuota] = " & r.Quotas.ToString.Replace(",", ".") _
                        & " ,[SumOfMaxBonus] = " & r.MaxBonus.ToString.Replace(",", ".") _
                        & " WHERE [Oid] = '" & EvDC.ToString & "'", conn)

                    cmd.ExecuteNonQuery()
                Else
                    ' new row
                    Dim cmd As New SqlCommand("INSERT INTO [dbo].[DistrQuota] " _
                        & " ([Oid] " _
                        & " ,[Distr] " _
                        & " ,[EventsCatalog] " _
                        & " ,[SumOfQuota] " _
                        & " ,[SumOfMaxBonus]) " _
                      & " VALUES " _
                        & " ('" & OidF.ToString & "'" _
                        & " ,'" & r.distrOid & "'" _
                        & " ,'" & r.EventOid & "'" _
                        & " , " & r.Quotas.ToString.Replace(",", ".") _
                        & " , " & r.MaxBonus.ToString.Replace(",", ".") & ")", conn)

                    cmd.ExecuteNonQuery()
                End If

            Catch ex As Exception
                SharedProcc.TestWrite(Now & " ------- " & " InsertDistrQuotas " & " --- " & ex.Message)
            Finally
                conn.Close()
                sqlConn1.Close()
            End Try

        Next


    End Sub

End Class

Public Class CompressionUtils
    Private Const RatesArraySize As Integer = 256
    Private Const RangeWidth As Single = 0.1F
    Private Shared Version2Prefix As New Guid("DA088B12-6641-413b-BBFC-2829752DCF96")
    Private Const Version2XafCompressedYesString As String = "+"
    Private Const Version2XafCompressedNoString As String = "-"
    Private Const MinAlwaysCompressedLenght As Integer = 1000000

    Private Shared Function IsGoodCompressionForecast(data As MemoryStream) As Boolean
        If data IsNot Nothing Then
            Dim rates As Integer() = New Integer(RatesArraySize - 1) {}
            Dim inRange As Integer = 0
            Dim usedBytes As Integer = 0
            For i As Integer = 0 To RatesArraySize - 1
                rates(i) = 0
            Next
            While data.Position <> data.Length
                rates(data.ReadByte()) += 1
            End While
            For i As Integer = 0 To RatesArraySize - 1
                If rates(i) > 0 Then
                    usedBytes += 1
                End If
            Next
            Dim lowBoundary As Integer = CInt((data.Length / usedBytes) * (1 - RangeWidth / 2))
            Dim highBoundary As Integer = CInt((data.Length / usedBytes) * (1 + RangeWidth / 2))
            For i As Integer = 0 To RatesArraySize - 1
                If rates(i) > lowBoundary AndAlso rates(i) < highBoundary Then
                    inRange += 1
                End If
            Next
            If inRange < CInt(usedBytes * RangeWidth) Then
                Return True
            End If
        End If
        Return False
    End Function

    Private Shared Sub WriteHeader(isCompressed As Boolean, result As MemoryStream)
        Dim versionPrefix As Byte() = Version2Prefix.ToByteArray()
        Dim headerString As String = If(isCompressed, Version2XafCompressedYesString, Version2XafCompressedNoString)
        Dim header As Byte() = System.Text.Encoding.UTF8.GetBytes(headerString.ToCharArray())
        result.Write(versionPrefix, 0, versionPrefix.Length)
        result.Write(header, 0, header.Length)
    End Sub

    Private Shared Function CreateVersion2CompressedStream(compressed As MemoryStream, isCompressed As Boolean) As MemoryStream
        Dim result As New MemoryStream()
        WriteHeader(isCompressed, result)
        compressed.WriteTo(result)
        Return result
    End Function

    Private Shared Function DecompressData(ms As MemoryStream) As MemoryStream
        Dim BufferSize As Integer = 5196
        Dim result As New MemoryStream()
        Using inStream As New GZipStream(ms, CompressionMode.Decompress, True)
            Dim buffer As Byte() = New Byte(BufferSize - 1) {}
            While True
                Dim readCount As Integer = inStream.Read(buffer, 0, BufferSize)
                If readCount = 0 Then
                    Exit While
                End If
                result.Write(buffer, 0, readCount)
            End While
        End Using
        Return result
    End Function

    Private Shared Function DecompressVersion2Stream(ms As MemoryStream) As MemoryStream
        Dim header As Byte() = New Byte(System.Text.Encoding.UTF8.GetBytes(Version2XafCompressedYesString.ToCharArray()).Length - 1) {}
        ms.Read(header, 0, header.Length)
        Dim headerString As String = System.Text.Encoding.UTF8.GetString(header, 0, header.Length)
        If headerString = Version2XafCompressedYesString Then
            Return DecompressData(ms)
        End If
        If headerString = Version2XafCompressedNoString Then
            Dim result As New MemoryStream()
            While ms.Position < ms.Length
                result.WriteByte(CByte(ms.ReadByte()))
            End While
            Return result
        End If
        Throw New ArgumentException()
    End Function

    Public Shared Function Compress(data As MemoryStream) As MemoryStream
        If data IsNot Nothing AndAlso data.Length > 0 Then
            If (data.Length < MinAlwaysCompressedLenght) OrElse IsGoodCompressionForecast(data) Then
                Using compressed As New MemoryStream()
                    Using deflater As New GZipStream(compressed, CompressionMode.Compress, True)
                        data.WriteTo(deflater)
                    End Using
                    If compressed.Length < data.Length Then
                        Return CreateVersion2CompressedStream(compressed, True)
                    End If
                End Using
            End If
            Return CreateVersion2CompressedStream(data, False)
        End If
        Return data
    End Function

    Public Shared Function Decompress(data As MemoryStream) As MemoryStream
        If data IsNot Nothing AndAlso data.Length > 0 Then
            Dim startPosition As Long = data.Position
            Dim guidPrefix As Byte() = New Byte(15) {}
            data.Read(guidPrefix, 0, guidPrefix.Length)
            If New Guid(guidPrefix) = Version2Prefix Then
                Return DecompressVersion2Stream(data)
            Else
                data.Position = startPosition
                Return DecompressData(data)
            End If
        End If
        Return data
    End Function
    '#Region "Obsolete 9.1"
    '    Private Const BufferSize As Integer = 5196
    '    Private Class ReadItem
    '        Public Buffer As Byte() = New Byte(BufferSize - 1) {}
    '        Public Length As Integer = 0
    '    End Class
    '    <Obsolete("Use 'Compress' instead."), Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> _
    '    Public Shared Function CompressData(data As Byte()) As Byte()
    '        If data IsNot Nothing Then
    '            Using ms As New MemoryStream()
    '                Using deflater As New GZipStream(ms, CompressionMode.Compress, True)
    '                    deflater.Write(data, 0, data.Length)
    '                End Using
    '                Return ms.ToArray()
    '            End Using
    '        Else
    '            Return Nothing
    '        End If
    '    End Function
    '    <Obsolete("Use 'Decompress' instead."), Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> _
    '    Public Shared Function DecompressData(data As Byte()) As Byte()
    '        Dim result As Byte() = Nothing
    '        If data IsNot Nothing Then
    '            Dim dataBlocks As New ArrayList()
    '            Dim ms As New MemoryStream(data)
    '            Dim inStream As New GZipStream(ms, CompressionMode.Decompress)
    '            Dim pos As Integer = 0
    '            While True
    '                Dim readItem As New ReadItem()
    '                readItem.Length = inStream.Read(readItem.Buffer, 0, BufferSize)
    '                If readItem.Length <= 0 Then
    '                    Exit While
    '                End If
    '                dataBlocks.Add(readItem)
    '                pos += readItem.Length
    '            End While
    '            result = New Byte(pos - 1) {}
    '            Dim resultPosition As Integer = 0
    '            For i As Integer = 0 To dataBlocks.Count - 1
    '                Array.Copy(DirectCast(dataBlocks(i), ReadItem).Buffer, 0, result, resultPosition, DirectCast(dataBlocks(i), ReadItem).Length)
    '                resultPosition += DirectCast(dataBlocks(i), ReadItem).Length
    '            Next
    '        End If
    '        Return result
    '    End Function
    '#End Region
End Class

'Public Class CompressionConverter
'    Inherits ValueConverter
'    Public Overrides Function ConvertToStorageType(value As Object) As Object
'        If value IsNot Nothing AndAlso Not (TypeOf value Is Byte()) Then
'            Throw New ArgumentException()
'        End If
'        If value Is Nothing OrElse DirectCast(value, Byte()).Length = 0 Then
'            Return value
'        End If
'        Return CompressionUtils.Compress(New MemoryStream(DirectCast(value, Byte()))).ToArray()
'    End Function
'    Public Overrides Function ConvertFromStorageType(value As Object) As Object
'        If value IsNot Nothing AndAlso Not (TypeOf value Is Byte()) Then
'            Throw New ArgumentException()
'        End If
'        If value Is Nothing OrElse DirectCast(value, Byte()).Length = 0 Then
'            Return value
'        End If
'        Return CompressionUtils.Decompress(New MemoryStream(DirectCast(value, Byte()))).ToArray()
'    End Function
'    Public Overrides ReadOnly Property StorageType() As Type
'        Get
'            Return GetType(Byte())
'        End Get
'    End Property
'End Class




