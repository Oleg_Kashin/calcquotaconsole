﻿Imports System.IO
Imports OfficeOpenXml

Public Class Calc


    Dim sp As New SharedProcc
    Private lResult As New List(Of NPQuotaCalc)
    Private lResultNPSellOut As New List(Of NPDistr)
    Private lResultNPSKUSellOut As New List(Of NPSKU)
    Private lResultNPDSFSellOut As New List(Of NPDistrDSF)
    Private sd As Date
    Public Sub Procc()

        '  Dim objectsToProcess As New ArrayList(e.SelectedObjects)

     

        Dim DicNP As New Dictionary(Of String, NPDistr)
        Dim DicNPDSF As New Dictionary(Of String, NPDistrDSF)

        'Dim DicNPSellOut As New Dictionary(Of String, NPDistr)
        'Dim DicNPDSFSellOut As New Dictionary(Of String, NPDistrDSF)


        Dim lRateSAP As New List(Of SAPRate)
        Dim DicRateSAP As New Dictionary(Of String, SAPRate)
        Dim listSellOut As New List(Of ItemNakl)
        Dim DicSellOut As New Dictionary(Of String, List(Of ItemNakl))
        Dim EventID As String = ""
        Dim koeff As Double = 0 ' koeff
        Dim b As Boolean = False
        Dim bb As Boolean = True
        Dim bRATE As Boolean = False
        Dim dicSubCategDiscount As New Dictionary(Of String, Double)

        sp.loadCalcQuotaNP(b, bRATE)
        sd = New Date(sp.y, sp.m, 1).AddMonths(-2) ' 1)

        If b Or bRATE Then

            Dim mm, dd, yy As String

            Dim dCalc As Date = Now
            If sp.SpravDate = Nothing Then
                If dCalc.Day < 10 Then
                    dd = "0" & dCalc.Day
                Else
                    dd = dCalc.Day
                End If
                If dCalc.Month < 10 Then
                    mm = "0" & dCalc.Month
                Else
                    mm = dCalc.Month
                End If
                yy = dCalc.Year
            Else
                If sp.SpravDate.Day < 10 Then
                    dd = "0" & sp.SpravDate.Day
                Else
                    dd = sp.SpravDate.Day
                End If
                If sp.SpravDate.Month < 10 Then
                    mm = "0" & sp.SpravDate.Month
                Else
                    mm = sp.SpravDate.Month
                End If
                yy = sp.SpravDate.Year
            End If


            sp.Path = My.Settings.PathSprav & dd & mm & yy & "\"

            ' sp.Path = My.Settings.PathSprav & 28 & "01" & 2016 & "\"

            Try
                sp.CheckFact(sp.Path) ' , My.Settings.PathFact0)
            Catch ex As Exception
                SharedProcc.TestWrite(Now & " CheckFact " & ex.Message)
            End Try

            sp.loadEC()
            sp.loadDistr()

            If IO.File.Exists(sp.Path & "InvestmentType.txt") Then
                Console.WriteLine(Now & " InvestmentType " & sp.Path & "InvestmentType.txt")
                SharedProcc.TestWrite(Now & " InvestmentType " & sp.Path & "InvestmentType.txt")
                sp.ImportTxtInvType(sp.Path & "InvestmentType.txt") ' Users.txt
            Else
                SharedProcc.TestWrite(Now & " файл InvestmentType.txt не найден")
                bb = False
            End If

            If IO.File.Exists(sp.Path & "TTDetails.txt") Then
                Console.WriteLine(Now & " TTDetails " & sp.Path & "TTDetails.txt")
                SharedProcc.TestWrite(Now & " TTDetails " & sp.Path & "TTDetails.txt")
                sp.ImportTxtTTDetails(sp.Path & "TTDetails.txt") ' Users.txt
            Else
                SharedProcc.TestWrite(Now & " файл TTDetails.txt не найден")
                bb = False
            End If

            sp.loadSubCateg()

            If IO.File.Exists(sp.Path & "NationalProgramSKU.txt") Then
                Console.WriteLine(Now & " NationalProgramSKU " & sp.Path & "NationalProgramSKU.txt")
                SharedProcc.TestWrite(Now & " NationalProgramSKU " & sp.Path & "NationalProgramSKU.txt")
                sp.ImportTxtNationalProgramSKU(sp.Path & "NationalProgramSKU.txt") ' Users.txt
            Else
                SharedProcc.TestWrite(Now & " файл NationalProgramSKU.txt не найден")
                bb = False
            End If


            If IO.File.Exists(sp.Path & "SellIn.txt") Then
                Console.WriteLine(Now & " SellIn " & sp.Path & "SellIn.txt")
                SharedProcc.TestWrite(Now & " SellIn " & sp.Path & "SellIn.txt")
                sp.ImportTxtSellIn(sp.Path & "SellIn.txt") ' Users.txt
            Else
                SharedProcc.TestWrite(Now & " файл SellIn.txt не найден")
                bb = False
            End If

            If IO.File.Exists(sp.Path & "DistrSAPCodesChanges.txt") Then
                Console.WriteLine(Now & " DistrSAPCodesChanges " & sp.Path & "DistrSAPCodesChanges.txt")
                SharedProcc.TestWrite(Now & " DistrSAPCodesChanges " & sp.Path & "DistrSAPCodesChanges.txt")
                sp.ImportTxtDistrSAPCodes(sp.Path & "DistrSAPCodesChanges.txt") ' Users.txt
            Else
                SharedProcc.TestWrite(Now & " файл DistrSAPCodesChanges.txt не найден")
                bb = False
            End If

            sp.loadQuotaNPKoeff(koeff)

            For Each ec In sp.LEventsCatalog ' .Where(Function(ff) ff.EventID = 748)
                If sp.dicNationalProgramSKU.ContainsKey(ec.EventID) Then
                    For Each it In sp.dicNationalProgramSKU.Item(ec.EventID)
                        Dim itCode As String = it.ToString
                        If sp.DicSubCateg.ContainsKey(itCode) Then
                            If Not dicSubCategDiscount.ContainsKey(sp.DicSubCateg.Item(itCode)) Then
                                dicSubCategDiscount.Add(sp.DicSubCateg.Item(itCode), ec.DiscountNP)
                            Else
                                If dicSubCategDiscount.Item(sp.DicSubCateg.Item(itCode)) < ec.DiscountNP Then
                                    dicSubCategDiscount.Item(sp.DicSubCateg.Item(itCode)) = ec.DiscountNP
                                End If
                            End If
                        End If
                    Next
                End If
            Next

            If koeff > 0 Then
                If bb Then
                    Console.WriteLine(Now & " calc")


                    For Each ec In sp.LEventsCatalog ' .Where(Function(ff) ff.EventID = 848)

                        If ec.DiscountNP > 0 Then

                            EventID = ec.EventID

                            lResultNPSellOut.Clear()
                            lResultNPSKUSellOut.Clear()
                            lResultNPDSFSellOut.Clear()
                            lResult.Clear()

                            If sp.dicNationalProgramSKU.ContainsKey(ec.EventID) Then

                                sd = ec.StartDate

                                For Each rd In sp.DDistr ' .Where(Function(ff) ff.Key.ToString = "RU00002540")

                                    Dim distr As String = rd.Key.ToString '  d.DistCode
                                    Dim distrSAP As String = "" ' d.DistrSAPActual
                                    Dim distrR As String = ""

                                    Try
                                        If sp.LSAP.Count > 0 Then
                                            If sp.LSAP.Where(Function(ff) ff.CodeOLD = distr And ff.DateCode <= sd).OrderByDescending(Function(ff) ff.DateCode).Count > 0 Then
                                                distrR = sp.LSAP.Where(Function(ff) ff.CodeOLD = distr And ff.DateCode <= sd).OrderByDescending(Function(ff) ff.DateCode).First.CodeNEW
                                            Else
                                                distrR = distr
                                            End If
                                        Else
                                            distrR = distr
                                        End If
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        If sp.LSAP.Where(Function(ff) ff.CodeOLD = distr).Count > 0 Then
                                            distrSAP = sp.LSAP.Where(Function(ff) ff.CodeOLD = distr).OrderByDescending(Function(ff) ff.DateCode).First.CodeNEW
                                        Else
                                            distrSAP = distr
                                        End If
                                    Catch ex As Exception

                                    End Try

                                    Dim distrN As String = rd.Value.NameDistributor

                                    If Not DicRateSAP.ContainsKey(distr) Then

                                        listSellOut.Clear()
                                        ' Console.WriteLine("Loadxml ")
                                        Try
                                            sp.LoadXMLforNP(My.Settings.PathFact, sd, distr, sp.dicTT, sp.dicTTDSF, listSellOut)
                                        Catch ex As Exception
                                            Console.WriteLine("Loadxml " & ex.Message)
                                            SharedProcc.TestWrite(Now & " Loadxml " & ex.Message)
                                        End Try

                                        'Export(listSellOut)
                                        'Console.WriteLine("Export")
                                        'Console.ReadKey()

                                        ' SAPRate

                                        Dim rs As New SAPRate
                                        rs.DistrCode = distr
                                        rs.DistrSAP = distrSAP
                                        rs.DistrRel = distrR
                                        rs.DistrName = distrN


                                        'If ec.EventsType = 109 Then
                                        '    npd.NP_Distr = Math.Round(IIf(npd.AmountTotal = 0, 0, npd.AmountNP / npd.AmountTotal) * 100, 1)
                                        'ElseIf ec.EventsType = 270 Then
                                        '    npd.NP_Distr = Math.Round(IIf(npd.AmountTotal = 0, 0, npd.AmountCSP_OPT / npd.AmountTotal) * 100, 1)
                                        'End If

                                        If Not DicNP.ContainsKey(109 & vbTab & distr) Then
                                            ' find NP Distr
                                            Dim npd As New NPDistr
                                            npd.DistrCode = distr
                                            npd.DistrSAP = distrSAP
                                            npd.DistrRel = distrR
                                            npd.DistrName = distrN
                                            npd.AmountTotal = listSellOut.Sum(Function(ff) ff.Summa)
                                            npd.AmountCSP = listSellOut.Where(Function(ff) ff.AdrDost = "CSP").Sum(Function(ff) ff.Summa)
                                            npd.AmountCSP_OPT = listSellOut.Where(Function(ff) ff.AdrDost = "CSP ( OPT)").Sum(Function(ff) ff.Summa)
                                            npd.AmountNOGTN = listSellOut.Where(Function(ff) ff.AdrDost = "NO GTN").Sum(Function(ff) ff.Summa)
                                            npd.AmountNP = listSellOut.Where(Function(ff) ff.AdrDost = "NP" Or ff.AdrDost = "NP, Loy").Sum(Function(ff) ff.Summa)
                                            npd.Amountblank = listSellOut.Where(Function(ff) ff.AdrDost = "blank").Sum(Function(ff) ff.Summa)

                                            rs.AmountTotal = npd.AmountTotal
                                            rs.AmountCSP_OPT = npd.AmountCSP_OPT
                                            rs.AmountNP = npd.AmountNP
                                            npd.NP_Distr = IIf(npd.AmountTotal = 0, 0, npd.AmountNP / npd.AmountTotal) * 100 '  Math.Round(IIf(npd.AmountTotal = 0, 0, npd.AmountNP / npd.AmountTotal) * 100, 1)
                                            rs.NP_Distr = npd.NP_Distr
                                            DicNP.Add(109 & vbTab & distr, npd)
                                        End If
                                        If Not DicNP.ContainsKey(270 & vbTab & distr) Then
                                            ' find NP Distr
                                            Dim npd1 As New NPDistr
                                            npd1.DistrCode = distr
                                            npd1.DistrSAP = distrSAP
                                            npd1.DistrRel = distrR
                                            npd1.DistrName = distrN
                                            npd1.AmountTotal = listSellOut.Sum(Function(ff) ff.Summa)
                                            npd1.AmountCSP = listSellOut.Where(Function(ff) ff.AdrDost = "CSP").Sum(Function(ff) ff.Summa)
                                            npd1.AmountCSP_OPT = listSellOut.Where(Function(ff) ff.AdrDost = "CSP ( OPT)").Sum(Function(ff) ff.Summa)
                                            npd1.AmountNOGTN = listSellOut.Where(Function(ff) ff.AdrDost = "NO GTN").Sum(Function(ff) ff.Summa)
                                            npd1.AmountNP = listSellOut.Where(Function(ff) ff.AdrDost = "NP" Or ff.AdrDost = "NP, Loy").Sum(Function(ff) ff.Summa)
                                            npd1.Amountblank = listSellOut.Where(Function(ff) ff.AdrDost = "blank").Sum(Function(ff) ff.Summa)

                                            rs.AmountTotal = npd1.AmountTotal
                                            rs.AmountCSP_OPT = npd1.AmountCSP_OPT
                                            rs.AmountNP = npd1.AmountNP
                                            npd1.NP_Distr = IIf(npd1.AmountTotal = 0, 0, npd1.AmountCSP_OPT / npd1.AmountTotal) * 100 '  Math.Round(IIf(npd1.AmountTotal = 0, 0, npd1.AmountCSP_OPT / npd1.AmountTotal) * 100, 1)
                                            rs.OPT_Distr = npd1.NP_Distr
                                            DicNP.Add(270 & vbTab & distr, npd1)
                                        End If


                                        'If ec.EventsType = 109 Then
                                        '    npdDSF.AmountNPDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "NP" Or ff.AdrDost = "NP, Loy") And ff.NomZakaz = "DSF").Sum(Function(ff) ff.Summa)
                                        '    npdDSF.AmountNPNoDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "NP" Or ff.AdrDost = "NP, Loy") And ff.NomZakaz = "NoDSF").Sum(Function(ff) ff.Summa)
                                        '    npdDSF.AmountNP = npd.AmountNP
                                        'ElseIf ec.EventsType = 270 Then
                                        '    npdDSF.AmountNPDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "CSP ( OPT)") And ff.NomZakaz = "DSF").Sum(Function(ff) ff.Summa)
                                        '    npdDSF.AmountNPNoDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "CSP ( OPT)") And ff.NomZakaz = "NoDSF").Sum(Function(ff) ff.Summa)
                                        '    npdDSF.AmountNP = npd.AmountCSP_OPT
                                        'End If


                                        If Not DicNPDSF.ContainsKey(109 & vbTab & distr) Then
                                            Dim npdDSF As New NPDistrDSF
                                            npdDSF.DistrCode = distr
                                            npdDSF.DistrSAP = distrSAP
                                            npdDSF.DistrRel = distrR
                                            npdDSF.DistrName = distrN
                                            npdDSF.AmountNPDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "NP" Or ff.AdrDost = "NP, Loy") And ff.NomZakaz = "DSF").Sum(Function(ff) ff.Summa)
                                            npdDSF.AmountNPNoDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "NP" Or ff.AdrDost = "NP, Loy") And ff.NomZakaz = "NoDSF").Sum(Function(ff) ff.Summa)
                                            If DicNP.ContainsKey(ec.EventsType & vbTab & distr) Then
                                                npdDSF.AmountNP = DicNP.Item(ec.EventsType & vbTab & distr).AmountNP
                                            End If
                                            DicNPDSF.Add(109 & vbTab & distr, npdDSF)
                                        End If

                                        If Not DicNPDSF.ContainsKey(270 & vbTab & distr) Then
                                            Dim npdDSF1 As New NPDistrDSF
                                            npdDSF1.DistrCode = distr
                                            npdDSF1.DistrSAP = distrSAP
                                            npdDSF1.DistrRel = distrR
                                            npdDSF1.DistrName = distrN
                                            npdDSF1.AmountNPDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "CSP ( OPT)") And ff.NomZakaz = "DSF").Sum(Function(ff) ff.Summa)
                                            npdDSF1.AmountNPNoDSF = listSellOut.Where(Function(ff) (ff.AdrDost = "CSP ( OPT)") And ff.NomZakaz = "NoDSF").Sum(Function(ff) ff.Summa)
                                            If DicNP.ContainsKey(ec.EventsType & vbTab & distr) Then
                                                npdDSF1.AmountNP = DicNP.Item(ec.EventsType & vbTab & distr).AmountCSP_OPT
                                            End If
                                            DicNPDSF.Add(270 & vbTab & distr, npdDSF1)
                                        End If

                                        DicRateSAP.Add(distr, rs)

                                        If Not DicSellOut.ContainsKey(distr) Then
                                            Dim lsu As New List(Of ItemNakl)
                                            For Each rr In listSellOut
                                                lsu.Add(rr)
                                            Next
                                            DicSellOut.Add(distr, lsu)
                                        End If
                                    End If
                                    ' SAPRate
                                    '  Console.WriteLine("SAPRate ")
                                    If DicRateSAP.ContainsKey(distr) Then

                                        If sp.dicNationalProgramSKU.ContainsKey(EventID) Then
                                            For Each it In sp.dicNationalProgramSKU.Item(EventID)

                                                Dim itCode As String = it.ToString

                                                If sp.DicSI.ContainsKey(distr) Then

                                                    Dim npqc As New NPQuotaCalc
                                                    npqc.EventID = EventID
                                                    npqc.EventNameRuss = ec.EventNameRuss
                                                    npqc.DistrCode = distr
                                                    npqc.DistrSAP = distrSAP
                                                    npqc.DistrRel = distrR
                                                    npqc.NameDistributor = distrN
                                                    npqc.ItemCode = itCode
                                                    If sp.DicProduct.ContainsKey(itCode) Then
                                                        npqc.SkuName = sp.DicProduct.Item(itCode)
                                                    Else
                                                        npqc.SkuName = itCode
                                                    End If
                                                    Dim npsku As New NPSKU
                                                    npsku.DistrCode = distr
                                                    npsku.DistrSAP = distrSAP
                                                    npsku.DistrRel = distrR
                                                    npsku.DistrName = distrN
                                                    npsku.ItemCode = itCode
                                                    If sp.DicProduct.ContainsKey(itCode) Then
                                                        npsku.SkuName = sp.DicProduct.Item(itCode)
                                                    Else
                                                        npsku.SkuName = itCode
                                                    End If

                                                    'If distr = "RU00000006" And itCode = "FTR22270" Then
                                                    '    Dim ddd As String = "sdfsd"
                                                    'End If

                                                    Dim arr(5) As Double
                                                    For rr As Integer = 0 To 5

                                                        If sp.DicSI.Item(distr).Where(Function(ff) ff.ItemCode = itCode And
                                                                                                         ff.Date1 >= sd.AddMonths(-7 + rr) And
                                                                                                                          ff.Date1 < sd.AddMonths(-6 + rr)).Count > 0 Then


                                                            arr(rr) = sp.DicSI.Item(distr).Where(Function(ff) ff.ItemCode = itCode And
                                                                                                            ff.Date1 = sd.AddMonths(-7 + rr) And
                                                                                                                          ff.Date1 < sd.AddMonths(-6 + rr)).First.Amount
                                                        Else
                                                            arr(rr) = 0
                                                        End If


                                                        Select Case rr
                                                            Case 0
                                                                npqc.m1 = arr(rr)
                                                            Case 1
                                                                npqc.m2 = arr(rr)
                                                            Case 2
                                                                npqc.m3 = arr(rr)
                                                            Case 3
                                                                npqc.m4 = arr(rr)
                                                            Case 4
                                                                npqc.m5 = arr(rr)
                                                            Case 5
                                                                npqc.m6 = arr(rr)
                                                        End Select

                                                    Next

                                                    Try
                                                        npqc.Average = Aggregate si In arr.Where(Function(ff) ff > 0)
                                                                                 Into Average(si)
                                                    Catch ex As Exception
                                                        npqc.Average = 0
                                                    End Try


                                                    Try
                                                        npqc.BL = Aggregate si In arr.Where(Function(ff) ff > 0 And ff < npqc.Average)
                                                                                   Into Average(si)
                                                    Catch ex As Exception
                                                        npqc.BL = 0
                                                    End Try


                                                    If npqc.BL = 0 Then
                                                        npqc.BL_result = npqc.Average
                                                    Else
                                                        npqc.BL_result = npqc.BL
                                                    End If


                                                    ' find NP SKU
                                                    'npsku.AmountTotal = listSellOut.Where(Function(ff) ff.SKULoc = itCode).Sum(Function(ff) ff.Summa)
                                                    'npsku.AmountCSP = listSellOut.Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "CSP").Sum(Function(ff) ff.Summa)
                                                    'npsku.AmountCSP_OPT = listSellOut.Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "CSP ( OPT)").Sum(Function(ff) ff.Summa)
                                                    'npsku.AmountNOGTN = listSellOut.Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "NO GTN").Sum(Function(ff) ff.Summa)
                                                    'npsku.AmountNP = listSellOut.Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "NP").Sum(Function(ff) ff.Summa)
                                                    'npsku.Amountblank = listSellOut.Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "blank").Sum(Function(ff) ff.Summa)
                                                    If DicSellOut.ContainsKey(distr) Then
                                                        npsku.AmountTotal = DicSellOut.Item(distr).Where(Function(ff) ff.SKULoc = itCode).Sum(Function(ff) ff.Summa)
                                                        npsku.AmountCSP = DicSellOut.Item(distr).Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "CSP").Sum(Function(ff) ff.Summa)
                                                        npsku.AmountCSP_OPT = DicSellOut.Item(distr).Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "CSP ( OPT)").Sum(Function(ff) ff.Summa)
                                                        npsku.AmountNOGTN = DicSellOut.Item(distr).Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "NO GTN").Sum(Function(ff) ff.Summa)
                                                        npsku.AmountNP = DicSellOut.Item(distr).Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "NP").Sum(Function(ff) ff.Summa)
                                                        npsku.Amountblank = DicSellOut.Item(distr).Where(Function(ff) ff.SKULoc = itCode And ff.AdrDost = "blank").Sum(Function(ff) ff.Summa)
                                                    Else
                                                        npsku.AmountTotal = 0
                                                        npsku.AmountCSP = 0
                                                        npsku.AmountCSP_OPT = 0
                                                        npsku.AmountNOGTN = 0
                                                        npsku.AmountNP = 0
                                                        npsku.Amountblank = 0
                                                    End If



                                                    If ec.EventsType = 109 Then
                                                        npsku.NP_SKU = Math.Round(IIf(npsku.AmountTotal = 0, 0, npsku.AmountNP / npsku.AmountTotal) * 100, 2)
                                                    ElseIf ec.EventsType = 270 Then
                                                        npsku.NP_SKU = Math.Round(IIf(npsku.AmountTotal = 0, 0, npsku.AmountCSP_OPT / npsku.AmountTotal) * 100, 2)
                                                    End If


                                                    npqc.Koeff = koeff
                                                    npqc.NP_SKU = npsku.NP_SKU

                                                    If DicNP.ContainsKey(ec.EventsType & vbTab & distr) Then
                                                        npqc.NP_Distr = DicNP.Item(ec.EventsType & vbTab & distr).NP_Distr
                                                    Else
                                                        npqc.NP_Distr = 0
                                                    End If

                                                    If DicNPDSF.ContainsKey(ec.EventsType & vbTab & distr) Then
                                                        npqc.DSF_Share = DicNPDSF.Item(ec.EventsType & vbTab & distr).DSF_Share
                                                    Else
                                                        npqc.DSF_Share = 0
                                                    End If


                                                    npqc.DiscountNP = ec.DiscountNP

                                                    If ec.EventsType = 109 Then
                                                        npqc.NP_Result = IIf(npqc.NP_SKU > 100, npqc.NP_Distr, IIf(npqc.NP_SKU > npqc.NP_Distr, npqc.NP_SKU, npqc.NP_Distr))
                                                    ElseIf ec.EventsType = 270 Then
                                                        npqc.NP_Result = npqc.NP_Distr
                                                        npqc.DSF_Share = 100
                                                    End If

                                                    ' npqc.distrOid = distrOid
                                                    lResult.Add(npqc)

                                                    lResultNPSKUSellOut.Add(npsku)

                                                End If
                                            Next

                                        End If

                                        If DicNPDSF.ContainsKey(ec.EventsType & vbTab & distr) Then
                                            Dim npdDSF As New NPDistrDSF
                                            npdDSF.DistrCode = distr
                                            npdDSF.DistrSAP = distrSAP
                                            npdDSF.DistrRel = distrR
                                            npdDSF.DistrName = distrN
                                            npdDSF.AmountNPDSF = DicNPDSF.Item(ec.EventsType & vbTab & distr).AmountNPDSF
                                            npdDSF.AmountNPNoDSF = DicNPDSF.Item(ec.EventsType & vbTab & distr).AmountNPNoDSF
                                            npdDSF.AmountNP = DicNPDSF.Item(ec.EventsType & vbTab & distr).AmountNP
                                            lResultNPDSFSellOut.Add(npdDSF)
                                        End If

                                        If DicNP.ContainsKey(ec.EventsType & vbTab & distr) Then
                                            Dim npd As New NPDistr
                                            npd.DistrCode = distr
                                            npd.DistrSAP = distrSAP
                                            npd.DistrRel = distrR
                                            npd.DistrName = distrN
                                            npd.AmountTotal = DicNP.Item(ec.EventsType & vbTab & distr).AmountTotal
                                            npd.AmountCSP = DicNP.Item(ec.EventsType & vbTab & distr).AmountCSP
                                            npd.AmountCSP_OPT = DicNP.Item(ec.EventsType & vbTab & distr).AmountCSP_OPT
                                            npd.AmountNOGTN = DicNP.Item(ec.EventsType & vbTab & distr).AmountNOGTN
                                            npd.AmountNP = DicNP.Item(ec.EventsType & vbTab & distr).AmountNP
                                            npd.Amountblank = DicNP.Item(ec.EventsType & vbTab & distr).Amountblank
                                            npd.NP_Distr = DicNP.Item(ec.EventsType & vbTab & distr).NP_Distr

                                            lResultNPSellOut.Add(npd)

                                        End If

                                    End If

                                Next   ' по дистрам
                            Else
                                SharedProcc.TestWrite(Now & " ------- " & " по программе ID " & ec.EventID & " не найдены NationalProgramSKU.")
                            End If

                            '  Console.WriteLine("ExportExcel ")
                            If b Then
                                ExportExcel(ec)
                            End If

                        Else
                            SharedProcc.TestWrite(Now & " ------- " & " по программе ID " & ec.EventID & " не заполнено поле DiscountNP.")
                        End If

                    Next  ' по программам ec

                    ' SAPRate

                    For Each rd In sp.DDistr ' '.Where(Function(ff) ff.Key.ToString = "RU00000006")

                        Dim distr As String = rd.Key.ToString

                        Dim AmountSellInDistr As Double
                        Dim AmountSubCateg As Double
                        Dim AmountSubCategN As Double
                        For Each r In sp.DicSubCategR
                            Dim sc As String = r.Key.ToString
                            AmountSubCateg = 0
                            AmountSubCategN = 0
                            AmountSellInDistr = 0

                            If sp.DicSI.ContainsKey(distr) Then

                                If sp.DicSI.Item(distr).Where(Function(ff) ff.Date1 >= sd.AddMonths(-7) And
                                                                           ff.Date1 < sd.AddMonths(-1)).Count > 0 Then


                                    AmountSellInDistr = sp.DicSI.Item(distr).Where(Function(ff) ff.Date1 >= sd.AddMonths(-7) And
                                                                           ff.Date1 < sd.AddMonths(-1)).Sum(Function(ff) ff.Amount)

                                End If


                                If sp.DicSI.Item(distr).Where(Function(ff) ff.SubCategID = sc And
                                                                                           ff.Date1 >= sd.AddMonths(-7) And
                                                                                                            ff.Date1 < sd.AddMonths(-1)).Count > 0 Then


                                    AmountSubCateg = sp.DicSI.Item(distr).Where(Function(ff) ff.SubCategID = sc And
                                                                                           ff.Date1 >= sd.AddMonths(-7) And
                                                                                                            ff.Date1 < sd.AddMonths(-1)).Sum(Function(ff) ff.Amount)

                                    If AmountSubCateg > 0 Then
                                        If sp.DicSI.Item(distr).Where(Function(ff) ff.SubCategID = sc And
                                                                             ff.National = True And
                                                                             ff.Date1 >= sd.AddMonths(-7) And
                                                                             ff.Date1 < sd.AddMonths(-1)).Count > 0 Then

                                            AmountSubCategN = sp.DicSI.Item(distr).Where(Function(ff) ff.SubCategID = sc And
                                                                                            ff.National = True And
                                                                                            ff.Date1 >= sd.AddMonths(-7) And
                                                                                            ff.Date1 < sd.AddMonths(-1)).Sum(Function(ff) ff.Amount)

                                            Dim rs As New SAPRate
                                            If DicRateSAP.ContainsKey(distr) Then
                                                rs.DistrCode = distr
                                                rs.DistrSAP = DicRateSAP.Item(distr).DistrSAP
                                                rs.DistrRel = DicRateSAP.Item(distr).DistrRel
                                                rs.DistrName = DicRateSAP.Item(distr).DistrName
                                                rs.NP_Distr = DicRateSAP.Item(distr).NP_Distr
                                                rs.OPT_Distr = DicRateSAP.Item(distr).OPT_Distr
                                                rs.AmountTotal = DicRateSAP.Item(distr).AmountTotal
                                                rs.AmountNP = DicRateSAP.Item(distr).AmountNP
                                                rs.AmountCSP_OPT = DicRateSAP.Item(distr).AmountCSP_OPT
                                            End If

                                            If sp.DDistr.ContainsKey(distr) Then
                                                rs.Target = sp.DDistr.Item(distr).Target
                                            Else
                                                rs.Target = 0
                                            End If

                                            rs.SubCateg = r.Key.ToString
                                            If sp.DicSubCategN.ContainsKey(r.Key.ToString) Then
                                                rs.SubCategName = sp.DicSubCategN.Item(r.Key.ToString)
                                            Else
                                                rs.SubCategName = ""
                                            End If
                                            If dicSubCategDiscount.ContainsKey(rs.SubCateg) Then
                                                rs.MaxDiscountSubCateg = dicSubCategDiscount.Item(rs.SubCateg)
                                            Else
                                                rs.MaxDiscountSubCateg = 0
                                            End If
                                            rs.SumofInvoicedSales = AmountSubCateg
                                            rs.SumofInvoicedSalesN = AmountSubCategN

                                            rs.AmountSellIn = AmountSellInDistr

                                            If rs.Rate_for_RA > 0 Then
                                                lRateSAP.Add(rs)
                                            End If
                                        End If


                                    End If
                                End If
                            End If
                        Next
                        ' SAPRate

                    Next


                    ' ------- SAP Rate

                    Try
                        If lRateSAP.Count > 0 Then
                            ExportExcelSAP1(lRateSAP)
                        End If
                    Catch ex As Exception
                        SharedProcc.TestWrite(Now & " ------- " & " ExportExcelSAP1 " & ex.Message)
                    End Try

                    ' ------- SAP Rate

                End If
            Else
                SharedProcc.TestWrite(Now & " EventsCatalog " & " не найден коэффициент NP (пункт меню 'Справочник Колгейт' -> 'NP Quota'.")
            End If

        End If

    End Sub

    Private Sub ExportExcelSAP1(ByVal lRateSAP As List(Of SAPRate))

        Dim p As String = ""
        Try
            p = IO.Path.GetTempFileName()
        Catch ex As IOException
            ' выдать сообщение
        End Try

        If File.Exists(p) Then
            File.Delete(p)
        End If

        If p <> "" Then
            Dim newFile As New FileInfo(p)
            Using pck As New ExcelPackage(newFile)

                Dim sh As String = "расчет SAP rate"

                Dim wsData = pck.Workbook.Worksheets.Add(sh)

                Dim dataRange = wsData.Cells("A2").LoadFromCollection((From s In lRateSAP
                                                                       Order By s.DistrCode, s.SubCateg
                                                                  Select s), False, Table.TableStyles.None) ' OfficeOpenXml.Table.TableStyles.Medium2)


                wsData.Cells.AutoFitColumns()

                ' Headers
                wsData.Cells("A1").Value = "Дистрибьютор Код"
                wsData.Cells("B1").Value = "Дистрибьютор Код SAP (QV, Cislink)"
                wsData.Cells("C1").Value = "Дистрибьютор Код релевантный"
                wsData.Cells("D1").Value = "Дистрибьютор Наименование"
                wsData.Cells("E1").Value = "СабКатегория"
                wsData.Cells("F1").Value = "СабКатегория Название"
                wsData.Cells("G1").Value = "Макс скидка на промо СКЮ в данной сабкатегории"
                wsData.Cells("H1").Value = "Селл ин - Sum of Invoiced Sales за предыдущие 6 мес"
                wsData.Cells("I1").Value = "Селл ин - Sum of Invoiced Sales за предыдущие 6 мес ПРОМО"
                wsData.Cells("J1").Value = "% SKU in SubCat"
                wsData.Cells("K1").Value = "% NP Distr"
                wsData.Cells("L1").Value = "% OPT Distr"
                wsData.Cells("M1").Value = "% (NP + OPT) Distr"
                wsData.Cells("N1").Value = "Продажи sell-out (CP Price)"
                wsData.Cells("O1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type OPT"
                wsData.Cells("P1").Value = "NP SOB" ' "PercentAmountCSP_OPT"
                wsData.Cells("Q1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NP"
                wsData.Cells("R1").Value = "OPT SOB" ' "PercentAmountNP"
                wsData.Cells("S1").Value = "Селл ин - Дистрибьютора"
                wsData.Cells("T1").Value = "SubCat SOB = доля промо сабкатегории в продажах Дистрибьютора"
                wsData.Cells("U1").Value = "Tar Type ID = 6"  ' Target
                wsData.Cells("V1").Value = "Tar Type ID = 6 by SubCat"      ' TarTypeID7bySubCat
                wsData.Cells("W1").Value = "rate for RA"
                wsData.Cells("X1").Value = "Estimation = сумма прогноза выплаты" 'Estimation

                wsData.Cells("H:I").Style.Numberformat.Format = "#,##0"
                wsData.Cells("J:M").Style.Numberformat.Format = "#,##0.0"
                wsData.Cells("N:O").Style.Numberformat.Format = "#,##0"
                wsData.Cells("P:P").Style.Numberformat.Format = "#,##0.0"
                wsData.Cells("Q:Q").Style.Numberformat.Format = "#,##0"
                wsData.Cells("R:R").Style.Numberformat.Format = "#,##0.0"
                wsData.Cells("S:S").Style.Numberformat.Format = "#,##0"
                wsData.Cells("T:T").Style.Numberformat.Format = "#,##0.0"
                wsData.Cells("U:V").Style.Numberformat.Format = "#,##0"
                wsData.Cells("X:X").Style.Numberformat.Format = "#,##0"


                wsData.Cells("A1:X1").Style.Font.Size = 8
                wsData.Cells("A1:X1").Style.WrapText = True
                wsData.Cells("A1:X1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

                Try
                    pck.Save()
                Catch ex As Exception
                    Console.WriteLine(ex.Message)
                End Try

            End Using

        End If

        Dim lNPRateSAP As New List(Of NPRAforSAP)

        For Each r In lRateSAP
            Dim np As New NPRAforSAP

            np.KOZGF = "ZRB1"
            np.KOLNR = "66"
            np.KOTABNR = "KOTE507"
            np.KSCHL = "ZR61"
            np.MODE = "C"
            np.SPLIT_IND = "Y"
            np.BOART = "ZRU1"
            np.VKORG = "RU01"
            np.VTWEG = "99"
            np.SPART = "99"
            np.BOTEXT = "Variable Indirect-RU"
            np.BONEM = r.DistrRel
            np.WAERS = "RUB"
            np.ZLSCH = ""
            np.ABREX = "Product Promo"
            np.DATAB = New Date(sd.Year, sd.Month, 1)
            np.DATBI = New Date(sd.Year, sd.Month, Date.DaysInMonth(sd.Year, sd.Month))
            np.KUNNR = r.DistrRel
            np.ZZPRODH2 = r.SubCateg
            np.KONP_KBETR = r.Rate_for_RA
            np.KONP_KBRUE = r.Rate_for_RA
            If sp.DicSubCategR.ContainsKey(r.SubCateg) Then
                np.BOMAT = sp.DicSubCategR.Item(r.SubCateg)
            Else
                np.BOMAT = ""
            End If
            np.KRECH = "A"

            lNPRateSAP.Add(np)

        Next

        Dim p1 As String = ""
        Try
            p1 = IO.Path.GetTempFileName()
        Catch ex As IOException
            ' выдать сообщение
        End Try

        If File.Exists(p1) Then
            File.Delete(p1)
        End If

        If p1 <> "" Then
            Dim newFile1 As New FileInfo(p1)
            Using pck1 As New ExcelPackage(newFile1)

                Dim sh As String = "NP RA for SAP"

                Dim wsData = pck1.Workbook.Worksheets.Add(sh)

                Dim dataRange = wsData.Cells("A1").LoadFromCollection((From s In lNPRateSAP
                                                                       Order By s.BONEM, s.ZZPRODH2
                                                                  Select s), True, Table.TableStyles.None) ' OfficeOpenXml.Table.TableStyles.Medium2)


                wsData.Cells.AutoFitColumns()

                wsData.Cells("S:S").Style.Numberformat.Format = "0"
                wsData.Cells("P:Q").Style.Numberformat.Format = "dd/mm/yyyy"
                wsData.Cells("A1:W1").Style.Font.Size = 8
                wsData.Cells("A1:W1").Style.WrapText = True
                wsData.Cells("A1:W1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

                Try
                    pck1.Save()
                Catch ex As Exception
                    Console.WriteLine(ex.Message)
                End Try

            End Using

        End If

        Try
            sp.SaveFile0(p, "NP расчет SAP rate.xlsx", p1, "NP RA for SAP load.xlsx")
        Catch ex As Exception
            SharedProcc.TestWrite(Now & " ------- " & " SaveFile0 " & ex.Message)
        End Try


    End Sub

    'Private Sub ExportExcelSAP2(ByVal lRateSAP As List(Of SAPRate))

    '    Dim lNPRateSAP As New List(Of SAPRate)

    '    For Each r In lRateSAP
    '        Dim np As New NPRAforSAP

    '        np.KOZGF = "ZRB1"
    '        np.KOLNR = "66"
    '        np.KOTABNR = "KOTE507"
    '        np.KSCHL = "ZR61"
    '        np.MODE = "C"
    '        np.SPLIT_IND = "Y"
    '        np.BOART = "ZRU1"
    '        np.VKORG = "RU01"
    '        np.VTWEG = "99"
    '        np.SPART = "99"
    '        np.BOTEXT = "Variable Indirect-RU"
    '        np.BONEM = r.DistrCode
    '        np.WAERS = "RUB"
    '        np.ZLSCH = ""
    '        np.ABREX = "Product Promo"
    '        np.DATAB = New Date(sd.Year, sd.Month, 1)
    '        np.DATBI = New Date(sd.Year, sd.Month, Date.DaysInMonth(sd.Year, sd.Month))
    '        np.KUNNR = r.DistrCode
    '        np.ZZPRODH2 = r.SubCateg
    '        np.KONP_KBETR = r.Rate_for_RA
    '        np.KONP_KBRUE = r.Rate_for_RA
    '        If sp.DicSubCategR.ContainsKey(r.SubCateg) Then
    '            np.BOMAT = sp.DicSubCategR.Item(r.SubCateg)
    '        Else
    '            np.BOMAT = ""
    '        End If
    '        np.KRECH = "A"

    '        lNPRateSAP.Add(np)

    '    Next

    '    Dim p As String = ""
    '    Try
    '        p = IO.Path.GetTempFileName()
    '    Catch ex As IOException
    '        ' выдать сообщение
    '    End Try

    '    If File.Exists(p) Then
    '        File.Delete(p)
    '    End If

    '    If p <> "" Then
    '        Dim newFile As New FileInfo(p)
    '        Using pck As New ExcelPackage(newFile)

    '            Dim sh As String = "NP RA for SAP"

    '            Dim wsData = pck.Workbook.Worksheets.Add(sh)

    '            Dim dataRange = wsData.Cells("A1").LoadFromCollection((From s In lNPRateSAP
    '                                                                   Order By s.DistrCode, s.SubCateg
    '                                                              Select s), True, Table.TableStyles.None) ' OfficeOpenXml.Table.TableStyles.Medium2)


    '            wsData.Cells.AutoFitColumns()


    '            wsData.Cells("A1:W1").Style.Font.Size = 8
    '            wsData.Cells("A1:W1").Style.WrapText = True
    '            wsData.Cells("A1:W1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

    '            Try
    '                pck.Save()
    '            Catch ex As Exception
    '                Console.WriteLine(ex.Message)
    '            End Try

    '        End Using

    '        Try
    '            sp.SaveFile01(p, "NP RA for SAP load_" & Now.Year & "_" & Now.Month & "_" & Now.Day & ".xlsx")
    '        Catch ex As Exception
    '            SharedProcc.TestWrite(Now & " ------- " & " SaveFile0 " & ex.Message)
    '        End Try

    '    End If

    'End Sub


    Private Sub ExportExcel(ByVal ec As EventsCatalog)

        Try
            If lResultNPSellOut.Count > 0 Or lResult.Count > 0 Then

                '  Dim tF As New TemplateFile

                '  Using OutD As New FileStream(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Sales/") & "test.csv", FileMode.Create) ' Sprav_Dic.Value.PathP & 
                Dim p As String = ""
                Try
                    p = IO.Path.GetTempFileName()
                Catch ex As IOException
                    ' выдать сообщение
                End Try

                If File.Exists(p) Then
                    File.Delete(p)
                End If

                If p <> "" Then

                    Dim newFile As New FileInfo(p)
                    Using pck As New ExcelPackage(newFile)
                        ' get the handle to the existing worksheet
                        Dim sh As String = ""

                        If lResultNPSellOut.Count > 0 Then

                            If ec.EventsType = 109 Then
                                sh = "1.% NP distr"
                            ElseIf ec.EventsType = 270 Then
                                sh = "1.% OPT distr"
                            End If

                            Dim wsData = pck.Workbook.Worksheets.Add(sh)

                            Dim dataRange = wsData.Cells("A2").LoadFromCollection((From s In lResultNPSellOut
                                                                                   Order By s.DistrCode
                                                                              Select s), False, Table.TableStyles.None) ' OfficeOpenXml.Table.TableStyles.Medium2)

                            wsData.Cells.AutoFitColumns()

                            ' Headers
                            wsData.Cells("A1").Value = "Дистрибьютор Код"
                            wsData.Cells("B1").Value = "Дистрибьютор Код SAP (QV, Cislink)"
                            wsData.Cells("C1").Value = "Дистрибьютор Код релевантный"
                            wsData.Cells("D1").Value = "Дистрибьютор Наименование"
                            wsData.Cells("E1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type CSP"
                            wsData.Cells("F1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type OPT"
                            wsData.Cells("G1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NOGTN"
                            wsData.Cells("H1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NP"
                            wsData.Cells("I1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type blank"
                            wsData.Cells("J1").Value = "Продажи sell-out (CP Price)"
                            If ec.EventsType = 109 Then
                                wsData.Cells("K1").Value = "Коэффициент NP distr - доля продаж канала Традиционной розницы (Investment type = NP; NP,Loy) в общих продажах sell-out дистрибьютора"
                            ElseIf ec.EventsType = 270 Then
                                wsData.Cells("K1").Value = "Коэффициент OPT distr - доля продаж каналов Субдистрибьюторы и Оптовики (Investment type = OPT) в общих продажах sell-out дистрибьютора"
                            End If


                            wsData.Cells("E:J").Style.Numberformat.Format = "#,##0"
                            wsData.Cells("K:K").Style.Numberformat.Format = "#,##0.0"

                            wsData.Cells("A1:K1").Style.Font.Size = 8
                            wsData.Cells("A1:K1").Style.WrapText = True
                            wsData.Cells("A1:K1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

                        End If

                        'If listSellOut.Count > 0 Then
                        '    Dim wsData = pck.Workbook.Worksheets.Add("2.% NP distr Details")
                        '    Dim dataRange = wsData.Cells("A1").LoadFromCollection((From s In listSellOut
                        '                                                               Order By s.DistrCode
                        '                                                          Select s), False, OfficeOpenXml.Table.TableStyles.Medium2)
                        'End If

                        If lResultNPSKUSellOut.Count > 0 Then

                            If ec.EventsType = 109 Then
                                sh = "2.% NP SKU"
                            ElseIf ec.EventsType = 270 Then
                                sh = "2.% OPT SKU"
                            End If

                            Dim wsData = pck.Workbook.Worksheets.Add(sh)

                            Dim dataRange = wsData.Cells("A2").LoadFromCollection((From s In lResultNPSKUSellOut
                                                                                   Order By s.DistrCode, s.ItemCode
                                                                              Select s), False, OfficeOpenXml.Table.TableStyles.None)
                            wsData.Cells.AutoFitColumns()

                            ' Headers
                            wsData.Cells("A1").Value = "Дистрибьютор Код"
                            wsData.Cells("B1").Value = "Дистрибьютор Код SAP (QV, Cislink)"
                            wsData.Cells("C1").Value = "Дистрибьютор Код релевантный"
                            wsData.Cells("D1").Value = "Дистрибьютор Наименование"
                            wsData.Cells("E1").Value = "Артикул Код"
                            wsData.Cells("F1").Value = "Артикул Наименование"
                            wsData.Cells("G1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type CSP"
                            wsData.Cells("H1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type OPT"
                            wsData.Cells("I1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NOGTN"
                            wsData.Cells("J1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NP"
                            wsData.Cells("K1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type blank"
                            wsData.Cells("L1").Value = "Продажи sell-out (CP Price)"
                            If ec.EventsType = 109 Then
                                wsData.Cells("M1").Value = "Коэффициент NP  SKU - доля продаж канала Традиционной розницы (Investment type = NP; NP,Loy) в общих продажах sell-out дистрибьютора в разрезе по СКЮ"
                            ElseIf ec.EventsType = 270 Then
                                wsData.Cells("M1").Value = "Коэффициент OPT  SKU - доля продаж каналов Субдистрибьюторы и Оптовики (Investment type = OPT) в общих продажах sell-out дистрибьютора в разрезе по СКЮ"
                            End If

                            wsData.Cells("G:L").Style.Numberformat.Format = "#,##0"
                            wsData.Cells("M:M").Style.Numberformat.Format = "#,##0.0"

                            wsData.Cells("A1:M1").Style.Font.Size = 8
                            wsData.Cells("A1:M1").Style.WrapText = True
                            wsData.Cells("A1:M1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

                        End If

                        If lResultNPDSFSellOut.Count > 0 Then

                            If ec.EventsType = 109 Then
                                sh = "3.% NP DSF"
                            ElseIf ec.EventsType = 270 Then
                                sh = "3.% OPT DSF"
                            End If

                            Dim wsData = pck.Workbook.Worksheets.Add(sh)


                            Dim dataRange = wsData.Cells("A2").LoadFromCollection((From s In lResultNPDSFSellOut
                                                                               Order By s.DistrCode
                                                                          Select s), False, OfficeOpenXml.Table.TableStyles.None)
                            wsData.Cells.AutoFitColumns()

                            ' Headers
                            wsData.Cells("A1").Value = "Дистрибьютор Код"
                            wsData.Cells("B1").Value = "Дистрибьютор Код SAP (QV, Cislink)"
                            wsData.Cells("C1").Value = "Дистрибьютор Код релевантный"
                            wsData.Cells("D1").Value = "Дистрибьютор Наименование"
                            If ec.EventsType = 109 Then
                                wsData.Cells("E1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NP в ТТ с покрытием DSF"
                                wsData.Cells("F1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NP в ТТ не покрываемых DSF"
                                wsData.Cells("G1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type NP"
                                wsData.Cells("H1").Value = "Коэффициент DSF - доля продаж ТТ с покрытием DSF в канале  Традиционная розница"
                            ElseIf ec.EventsType = 270 Then
                                wsData.Cells("E1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type OPT в ТТ с покрытием DSF"
                                wsData.Cells("F1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type OPT в ТТ не покрываемых DSF"
                                wsData.Cells("G1").Value = "Продажи sell-out (CP Price) в канале с типом Investment type OPT"
                                wsData.Cells("H1").Value = "Коэффициент DSF - доля продаж ТТ с покрытием DSF в каналах Субдистрибьюторы и Оптовики"
                            End If


                            wsData.Cells("E:G").Style.Numberformat.Format = "#,##0"
                            wsData.Cells("H:H").Style.Numberformat.Format = "#,##0.0"

                            wsData.Cells("A1:H1").Style.Font.Size = 8
                            wsData.Cells("A1:H1").Style.WrapText = True
                            wsData.Cells("A1:H1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

                        End If


                        If lResult.Count > 0 Then

                            If ec.EventsType = 109 Then
                                sh = "4.quota calc"
                            ElseIf ec.EventsType = 270 Then
                                sh = "4.quota calc"
                            End If

                            Dim wsData = pck.Workbook.Worksheets.Add(sh)

                            Dim dataRange = wsData.Cells("A2").LoadFromCollection((From s In lResult
                                                                        Select s), False, OfficeOpenXml.Table.TableStyles.None)

                            wsData.Cells.AutoFitColumns()

                            ' Headers
                            wsData.Cells("A1").Value = "Промо программа Код"
                            wsData.Cells("B1").Value = "Промо программа Наименование"
                            wsData.Cells("C1").Value = "Дистрибьютор Код"
                            wsData.Cells("D1").Value = "Дистрибьютор  Код SAP (QV, Cislink)"
                            wsData.Cells("E1").Value = "Дистрибьютор Код релевантный"

                            wsData.Cells("F1").Value = "Дистрибьютор Наименование"
                            wsData.Cells("G1").Value = "Артикул Код"
                            wsData.Cells("H1").Value = "Артикул Наименование"

                            wsData.Cells("I1").Value = "Продажи (Inv Sales) за " & Format(sd.AddMonths(-7), "MM/yyyy") ' "m1"
                            wsData.Cells("J1").Value = "Продажи (Inv Sales) за " & Format(sd.AddMonths(-6), "MM/yyyy")  ' "m2"
                            wsData.Cells("K1").Value = "Продажи (Inv Sales) за " & Format(sd.AddMonths(-5), "MM/yyyy")  ' "m3"
                            wsData.Cells("L1").Value = "Продажи (Inv Sales) за " & Format(sd.AddMonths(-4), "MM/yyyy")  ' "m4"
                            wsData.Cells("M1").Value = "Продажи (Inv Sales) за " & Format(sd.AddMonths(-3), "MM/yyyy")  ' "m5"
                            wsData.Cells("N1").Value = "Продажи (Inv Sales) за " & Format(sd.AddMonths(-2), "MM/yyyy")  ' "m6"
                            wsData.Cells("O1").Value = "Продажи (Inv Sales) средние за предыдущие 6 месяцев"
                            wsData.Cells("P1").Value = "Продажи (Inv Sales) бейзлайн за предыдущие 6 месяцев (расчет по ROI формуле)"
                            wsData.Cells("Q1").Value = "Продажи (Inv Sales) (если Продажи бейзлайн = 0, то Продажи средние)"
                            wsData.Cells("R1").Value = "Коэффециент Promo - увеличение Продаж бейзлан в промо период"
                            If ec.EventsType = 109 Then
                                wsData.Cells("S1").Value = "Коэффициент NP distr - доля продаж канала Традиционной розницы (Investment type = NP; NP,Loy) в общих продажах sell-out дистрибьютора"
                                wsData.Cells("T1").Value = "Коэффициент NP  SKU - доля продаж канала Традиционной розницы (Investment type = NP; NP,Loy) в общих продажах sell-out дистрибьютора в разрезе по СКЮ"
                                wsData.Cells("U1").Value = "Коэффициент NP  - наибольший из коэффициетов NP distr или NP SKU"
                                wsData.Cells("V1").Value = "Коэффициент DSF - доля продаж DSF точек в продажах в канале NP в в общих продажах sell-out дистрибьютора"
                                wsData.Cells("W1").Value = "Квота NP  = Продажи (Inv Sales) * Коэффициент Promo * Коэффициент NP * Коэффициент DSF"
                                wsData.Cells("Y1").Value = "Макс сумма бонуса = Квота NP * Скидка"
                            ElseIf ec.EventsType = 270 Then
                                wsData.Cells("S1").Value = "Коэффициент OPT distr - доля продаж каналов Субдистрибьюторы и Оптовики (Investment type = OPT) в общих продажах sell-out дистрибьютора"
                                wsData.Cells("T1").Value = "Коэффициент OPT SKU - доля продаж каналов Субдистрибьюторы и Оптовики (Investment type = OPT) в общих продажах sell-out дистрибьютора в разрезе по СКЮ"
                                wsData.Cells("U1").Value = "Коэффициент OPT - для каналов Субдистрибьюторы и Оптовики всегда коэффициет OPT distr"
                                wsData.Cells("V1").Value = "Коэффициент DSF - для каналов Субдистрибьюторы и Оптовики используется коэфициент 100"
                                wsData.Cells("W1").Value = "Квота OPT  = Продажи (Inv Sales) * Коэффециент Promo * Коэффициент OPT * Коэффициент DSF"
                                wsData.Cells("Y1").Value = "Макс сумма бонуса = Квота OPT * Скидка"
                            End If

                            wsData.Cells("X1").Value = "Скидка"


                            wsData.Cells("I:Q").Style.Numberformat.Format = "#,##0"

                            wsData.Cells("I1:N1").Style.Numberformat.Format = "mm/yyyy"
                            wsData.Cells("A1:Y1").Style.WrapText = True

                            wsData.Cells("W:W").Style.Numberformat.Format = "#,##0"
                            wsData.Cells("Y:Y").Style.Numberformat.Format = "#,##0"

                            wsData.Cells("S:V").Style.Numberformat.Format = "#,##0.0"
                            wsData.Cells("X:X").Style.Numberformat.Format = "#,##0.0"

                            wsData.Cells("A1:Y1").Style.Font.Size = 8
                            wsData.Cells("A1:Y1").Style.WrapText = True
                            wsData.Cells("A1:Y1").Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top

                        End If

                        Try

                            pck.Save()
                        Catch ex As Exception
                            Console.WriteLine(ex.Message)
                        End Try


                    End Using

                End If

                Dim n As String = ""
                If ec.EventsType = 109 Then
                    n = "NP_Quota_Calc_EventID_"
                ElseIf ec.EventsType = 270 Then
                    n = "OPT_Quota_Calc_EventID_"
                End If
                '  IO.File.Copy(p, n & EventID & ".xlsx")

                Try
                    sp.SaveFile(ec.Oid, p, n & ec.EventID & ".xlsx")
                Catch ex As Exception
                    SharedProcc.TestWrite(Now & " ------- " & " по программе ID " & ec.EventID & " не удалось сохранить файл SaveFile" & ex.Message)
                End Try

                Try
                    sp.SummDistrQuotas(ec.Oid, lResult)
                Catch ex As Exception
                    SharedProcc.TestWrite(Now & " ------- " & " по программе ID " & ec.EventID & " не удалось сохранить SummDistrQuotas" & ex.Message)
                End Try
                'tF.Document = ObjectSpace.CreateObject(Of FileData)()
                'tF.Document.LoadFromStream(n & EventID & ".xlsx", New FileStream(p, FileMode.Open))

                'DownloadFileData(tF.Document)
            End If

        Catch ex As Exception


        End Try

    End Sub

    'Private Sub Export(ByVal listSellOut As List(Of ItemNakl))
    '    ' ------- SAP Rate

    '    Try


    '        Dim p As String = ""
    '        Try
    '            p = IO.Path.GetTempFileName()
    '        Catch ex As IOException
    '            ' выдать сообщение
    '        End Try

    '        If File.Exists(p) Then
    '            File.Delete(p)
    '        End If

    '        If p <> "" Then
    '            Dim newFile As New FileInfo(p)
    '            Using pck As New ExcelPackage(newFile)

    '                Dim sh As String = "расчет SAP rate"

    '                Dim wsData = pck.Workbook.Worksheets.Add(sh)

    '                Dim dataRange = wsData.Cells("A2").LoadFromCollection((From s In listSellOut
    '                                                                       Order By s.CustIDLoc
    '                                                                  Select s), True, Table.TableStyles.None) ' OfficeOpenXml.Table.TableStyles.Medium2)


    '                wsData.Cells.AutoFitColumns()


    '                Try
    '                    pck.Save()
    '                Catch ex As Exception
    '                    Console.WriteLine(ex.Message)
    '                End Try

    '            End Using

    '            Try
    '                sp.SaveFile0(p, "NP RA for SAP load_" & Now.Year & "_" & Now.Month & "_" & Now.Day & ".xlsx")
    '            Catch ex As Exception
    '                SharedProcc.TestWrite(Now & " ------- " & " SaveFile0 " & ex.Message)
    '            End Try

    '        End If
    '    Catch ex As Exception

    '    End Try


    '    ' ------- SAP Rate

    'End Sub

End Class
