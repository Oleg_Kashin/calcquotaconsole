﻿Public Class SomeClasses

End Class

Public Class MesssageData
    Property DateStart As Date ' Начало Периода отправки данных
    Property DateEnd As Date ' Конец Периода отправки данных
    Property UrL As String ' Юр.лицо
    Property Sklad As String ' Склад
    Property ListItem As New List(Of ItemSKU) ' Справочник товаров 
    Property ListCust As New List(Of ItemCust) ' Справочник клиентов
    Property ListNakl As New List(Of ItemNakl) ' Строки накладных
    Property ListVozvrat As New List(Of ItemNakl) ' строки возвратов по накладным
    Property ListZakaz As New List(Of ItemNakl) ' строки заказов

    Public Sub New()

    End Sub
End Class

Public Class ItemSKU
    Property SKUY As Boolean ' Признак сегментации True -да
    Property SKU As String  ' Код товара Глобальный (если есть)
    Property SKULoc As String ' Код товара локальный
    Property NameSKU As String ' Наименование товара
    Property Categ As String ' Категория товара
    Property Segment As String ' Сегмент товара
    Property Gruppa As String ' Группа товара
    Property Gruppa2 As String ' Подгруппа товара
    Property Brend As String ' Бренд
    Property CenaSegm As String        '10
    Property EdIzm As String '11
    Property SKUGruppaUser As String '12
    Property SubBrend As String '13
    Property SubCenaSegm As String '14
    Property NewCateg As String '15
    Property NewSegment As String '16
    Property NewGruppa As String '17
    Property NewGruppa2 As String '18
    Property NewBrend As String '19
    Property NewSubBrend As String '20
    Property NewCenaSegm As String '21
    Property Size01 As String '22
    Property Size02 As String '23
    Property Size03 As String '24
    Property PropSKU13 As String '25
    Property PropSKU14 As String '26
    Property PropSKU15 As String '27
    Property PropSKU16 As String '28
    Property PropSKU17 As String '29
    Property PropSKU18 As String '30
    Property PropSKU19 As String '31
    Property PropSKU20 As String '32
    Property PropSKU21 As String '33
    Property PropSKU22 As String '34
    Property PropSKU23 As String '35
    Property PropSKU24 As String '36
    Property PropSKU25 As String '37
    Property PropSKU26 As String '38
    Property PropSKU27 As String '39
    Property PropSKU28 As String '40
    Property PropSKU29 As String '41
    Property PropSKU30 As String '42
    Property PropSKU31 As String '43
    Property PropSKU32 As String '44
    Property PropSKU33 As String '45

    Public Sub New()

    End Sub
End Class

Public Class ItemCust
    Property CustY As Boolean
    Property CustID As String ' Код Адреса доставки контрагента Глобальный (если есть)
    Property CustIDLoc As String ' Код Адреса доставки контрагента Локальный
    Property CustName As String ' Наименование клиента
    Property Depart As String ' Департамент
    Property Otdel As String ' Отдел
    Property Otdel2 As String ' Направление
    Property Otdel3 As String ' Бригада
    Property CustKateg1 As String ' Категория партнера 1 уровень
    Property CustKateg2 As String ' Категория партнера 2 уровень
    Property INN As Long ' ИНН
    Property KPP As Long 'КПП

    Property AdrName As String  ' address dostavki
    Property okrug As String
    Property okato As String
    Property rnOk As String
    Property typeN As String
    Property city As String
    Property ado As String
    Property rnC As String
    Property typestr As String
    Property str As String
    Property dom As String
    Property dom2 As String
    Property CustKateg21 As String
    Property CUST1 As String
    Property CUST2 As String
    Property CUST3 As String
    Property CUST4 As String
    Property CUST5 As String
    Property CUST6 As String
    Property CUST7 As String
    Property CUST8 As String
    Property CUST9 As String
    Property CUST10 As String
    Property CUST11 As String
    Property CUST12 As String
    Property CUST13 As String
    Property CUST14 As String
    Property CUST15 As String
    Property CUST16 As String
    Property CUST17 As String
    Property CUST18 As String
    Property CUST19 As String
    Property CUST20 As String


    Public Sub New()

    End Sub
End Class

Public Class ItemNakl

    Property DateNakl As Date ' Дата Накладной
    Property NomNakl As String ' Номер Накладной
    Property NomZakaz As String ' Номер заказа
    Property CustIDLoc As String ' Код Адреса доставки контрагента Локальный
    Property CustID As String ' Код клиента Глобальный (если есть)
    Property AdrDost As String ' Адрес доставки
    Property SKU As String ' Код товара Глобальный (если есть)
    Property SKULoc As String ' Код товара локальный
    Property EdIzm As String ' Единица измерения
    Property Kolvo As Double ' Количество в единицах измерения
    Property Koeff As Double ' Коэффициет пересчета в килограммы
    Property Summa As Double ' Сумма с НДС
    Property SummaNet As Double ' Сумма без НДС
    Property SummaCost As Double ' Сумма Себест. без Н

    Public Sub New()

    End Sub
End Class

Public Class EventsCatalog

    Property Oid As String
    Property EventID As String     ' EventID
    Property EventName As String   ' EventName
    Property EventNameRuss As String   ' EventNameRuss
    Property Quantity As Double   ' Quantity
    Property Bonus As String ' Bonus
    Property EventsType As String ' EventsType
    Property StartDate As Date ' StartDate
    Property EndDate As Date   ' EndDate
    Property DiscountNP As Double

End Class

Public Class NPDistr
    Property DistrCode As String
    Property DistrSAP As String
    Property DistrRel As String
    Property DistrName As String
    Property AmountCSP As Double
    Property AmountCSP_OPT As Double
    Property AmountNOGTN As Double
    Property AmountNP As Double
    Property Amountblank As Double
    Property AmountTotal As Double
    ' Property NP_Distr As Double = IIf(AmountTotal = 0, 0, AmountNP / AmountTotal)
    Property NP_Distr As Double

    'ReadOnly Property NP_Distr As Double
    '    Get
    '        Return Math.Round(IIf(AmountTotal = 0, 0, AmountNP / AmountTotal) * 100, 1)
    '    End Get
    'End Property
End Class


Public Class NPSKU
    Property DistrCode As String
    Property DistrSAP As String
    Property DistrRel As String
    Property DistrName As String
    Property ItemCode As String
    Property SkuName As String
    Property AmountCSP As Double
    Property AmountCSP_OPT As Double
    Property AmountNOGTN As Double
    Property AmountNP As Double
    Property Amountblank As Double
    Property AmountTotal As Double
    Property NP_SKU As Double
    'ReadOnly Property NP_SKU As Double
    '    Get
    '        Return Math.Round(IIf(AmountTotal = 0, 0, AmountNP / AmountTotal) * 100, 1)
    '    End Get
    'End Property
End Class


Public Class NPDistrDSF
    Property DistrCode As String
    Property DistrSAP As String
    Property DistrRel As String
    Property DistrName As String
    Property AmountNPDSF As Double
    Property AmountNPNoDSF As Double
    Property AmountNP As Double
    'Property DSF_Share As Double = IIf(AmountNP = 0, 0, AmountNPDSF / AmountNP)
    ReadOnly Property DSF_Share As Double
        Get
            Return Math.Round(IIf(AmountNP = 0, 0, AmountNPDSF / AmountNP) * 100, 2)
        End Get
    End Property


End Class


Public Class NPQuotaCalc
    'Property EventOid As String
    Property EventID As String
    Property EventNameRuss As String
    ' Property EventType As Integer
    ' Property distrOid As String
    Property DistrCode As String
    Property DistrSAP As String
    Property DistrRel As String
    Property NameDistributor As String
    Property ItemCode As String
    Property SkuName As String
    Property m1 As Double
    Property m2 As Double
    Property m3 As Double
    Property m4 As Double
    Property m5 As Double
    Property m6 As Double
    Property Average As Double
    Property BL As Double
    Property BL_result As Double
    Property Koeff As Double
    Property NP_Distr As Double
    Property NP_SKU As Double

    Property NP_Result As Double

    'ReadOnly Property NP_Result As Double
    '    Get
    '        Return IIf(NP_SKU > 100, NP_Distr, IIf(NP_SKU > NP_Distr, NP_SKU, NP_Distr))
    '    End Get
    'End Property


    Property DSF_Share As Double

    ReadOnly Property Quota As Double
        Get
            Return NP_Result / 100 * BL_result * Koeff * DSF_Share / 100
        End Get
    End Property

    Property DiscountNP As Double ' ???

    ReadOnly Property Max_Bonus As Double
        Get
            Return Quota * DiscountNP / 100
        End Get
    End Property
End Class

Public Class NPQuotaDistr
    Property EventOid As String
    Property EventID As String
    Property distrOid As String
    Property DistrCode As String

    Property Quotas As Double
    Property MaxBonus As Double ' ???

  
End Class

Public Class Distributers

    Property DistCode As String   ' SAP код дистрибутора , как он учтен на сервере Сислинк
    Property DistOid As String
    Property NameDistributor As String
    Property CustomerNamelegal As String   ' Имя Дистрибутора
    Property AddressLegal As String  ' Address Legal
    Property City As String  ' Город нахождения дистрибутора

    Property INN As String  ' INN
    Property KPP As String  ' KPP

    'Property HeadOfanOrganization As String  ' HeadOfanOrganization
    'Property ChiefAccountant As String  ' ChiefAccountant

    Property Position1() As String
    Property Name1() As String
    Property Position2() As String
    Property Name2() As String
    Property GTNProtokolData() As Date
    Property IPRekv() As String
    Property DogovorData() As Date
    Property Field4() As String
    Property Contract() As Date
    Property Field1() As String
    Property withoutVAT() As Integer
    Property NewGTN() As String
    Property ESN() As Integer
    Property Target As Double

End Class

Public Class DistrSellIn

    Property Oid As String
    Property Dist As String
    Property DistCode As String
    Property Item As String
    Property ItemCode As String
    Property SubCategID As String
    Property National As Boolean
    Property Y As Integer
    Property M As Integer
    Property Amount As Double
    ReadOnly Property Date1 As Date
        Get
            Return New Date(Y, M, 1)
        End Get
    End Property
End Class

Public Class SAPRate

    Property DistrCode As String
    Property DistrSAP As String
    Property DistrRel As String
    Property DistrName As String
    Property SubCateg As String
    Property SubCategName As String
    Property MaxDiscountSubCateg As Double
    Property SumofInvoicedSales As Double
    Property SumofInvoicedSalesN As Double


    ReadOnly Property PercentSKUInSubCat As Double
        Get
            Return IIf(SumofInvoicedSales <> 0, SumofInvoicedSalesN / SumofInvoicedSales * 100, 0)
        End Get
    End Property

    Property NP_Distr As Double
    Property OPT_Distr As Double
    ReadOnly Property NP_OPT_Distr As Double
        Get
            Return NP_Distr + OPT_Distr
        End Get
    End Property

    Property AmountTotal As Double
    Property AmountCSP_OPT As Double
    ReadOnly Property PercentAmountCSP_OPT As Double
        Get
            Return IIf(AmountTotal <> 0, AmountCSP_OPT / AmountTotal * 100, 0)
        End Get
    End Property
    Property AmountNP As Double
    ReadOnly Property PercentAmountNP As Double
        Get
            Return IIf(AmountTotal <> 0, AmountNP / AmountTotal * 100, 0)
        End Get
    End Property

    Property AmountSellIn As Double
    ReadOnly Property PercentSellInSubCateg As Double
        Get
            Return IIf(AmountSellIn <> 0, SumofInvoicedSales / AmountSellIn * 100, 0)
        End Get
    End Property

    Property Target As Double
    ReadOnly Property TarTypeID6bySubCat As Double
        Get
            Return PercentSellInSubCateg * Target / 100
        End Get
    End Property
    '  Property NP_SKU As Double
    'Property DSF_Share_NP As Double
    'Property DSF_Share_OPT As Double
    'ReadOnly Property DSF_Share_NP_OPT As Double
    '    Get
    '        Return DSF_Share_NP + DSF_Share_OPT
    '    End Get
    'End Property

    ReadOnly Property Rate_for_RA As Double
        Get
            If MaxDiscountSubCateg * PercentSKUInSubCat * NP_OPT_Distr / 100 / 100 > 0 Then
                If MaxDiscountSubCateg * PercentSKUInSubCat * NP_OPT_Distr / 100 / 100 < 0.01 Then
                    Return 0.01
                Else
                    Return Math.Round(MaxDiscountSubCateg * PercentSKUInSubCat * NP_OPT_Distr / 100 / 100, 2)
                End If
            Else
                Return 0
            End If
        End Get
    End Property

    ReadOnly Property Estimation As Double
        Get
            Return TarTypeID6bySubCat * Rate_for_RA / 100
        End Get
    End Property

End Class


Public Class SalesByMonth

    Property DistrCode As String  ' DistrCode
    Property ContractorSAPCode As String  ' ContractorSAPCode
    Property SRID As String  ' SRID
    Property MCID As String ' Код формата торговой точки
    Property ChID As String  ' ID сети в БД CISLink
    Property CatalogID As String     ' CatalogID
    Property Quantity As String   ' Quantity
    Property AmountDP As String   ' AmountDP
    Property Amount As String   ' Amount
    Property y As Integer   '  y
    Property m As Integer   ' m
    Property d As Integer   ' d

End Class

Public Class TT

    Property TTID As String  ' Код ТТ
    Property TTOid As String

    Property ContractorSAPCode As String  ' Код ТТ
    Property DistrCode As String  ' SAP код дистрибутора , как он учтен на сервере Сислинк
    Property DistrOid As String
    Property City As String  ' City
    Property TTNameRuss As String     ' Название торговой точки
    Property Address As String   ' Address
    Property Address0 As String   ' Address
    Property SpoAddress As String   ' SpoAddress
    Property MCID As String ' Код формата торговой точки
    ' Property MCIDCode As String ' Код формата торговой точки
    Property ChID As String  ' ID сети в БД CISLink
    Property ChOid As String
    Property TTType As Integer ' тип ТТ 0 - sale 1 - be
End Class

Public Class REMaster

    Property GroupRECode As String  ' Код объединяющей группы форматов
    Property GroupREName As String  ' Название объединяющей группы форматов на английском
    Property GroupRENameRus As String  ' Название объединяющей группы форматов на русском
    Property RECode As String     ' Код формата торговой точки
    Property REName As String   ' Название формата торговой точки на английском
    Property RENameRus As String ' Название формата торговой точки на русском


End Class

Public Class UserToDistr

    Property UserID As String  ' Код пользователя в базе Сислинк
    Property DistrCode As String  ' SAP код дистрибутора , как он учтен на сервере Сислинк
    Property OidUserID As String
    Property OidDistrCode As String

End Class

Public Class Catalog

    Property CatalogID As String     ' CatalogID
    Property SkuID As String   ' SkuID
    Property CISLink_Disabled As String   ' CISLink_Disabled

End Class

Public Class Products

    Property SkuID As String  ' ID товара CP в БД CISLink, первичный ключ
    Property ManfCode As String  ' Артикул продукта
    Property BarCode As String  ' Лидирующий артикул ,по которму ведется  суммарный учет продаж уникального продукта

    Property TextCode As String  ' Код единицы рекомендованного ассортимента
    Property SkuName As String  ' Название продукта
    Property MustSkuName As String  ' Название продукта обязательного ассортимента , включая промопачки
    Property ItemsPerCase As String     ' Количество штук в коробке
    Property SizeBI6levelhierarchy As String   ' Объем или вес упаковки

    Property CurrentPrice As Double   ' Цена по прайс листу Колгейт на дату выгрузки с сервера
    Property NewVariantCode As String   ' Код уровня варианта
    Property NewVariantBI6levelhierarchy As String   ' Название варианта на английском
    Property NewVariantBI6levelhierarchyRUS As String   ' Название варианта на русском
    Property NewSubBrandCode As String   ' Код уровня СабБренд
    Property NewSubBrandBI6levelhierarchy As String   ' Название СабБренд на английском
    Property NewSubBrandBI6levelhierarchyRUS As String   ' Название СабБренд на русском
    Property ECPPH3code As String   ' Код уровня ECP иерархии
    Property HierarchyEC As String   ' Название ECP иерархии на английском
    Property SubCategoryCode As String   ' Код СабКатегории
    Property SubCategory As String   ' Название СабКатегории на английском
    Property SubCategoryRUS As String  ' Название СабКатегории на русском
    Property CatogoryCode As String   ' Код Категории
    Property Categoryname As String   ' Название Категории

    Property Type As Integer ' POP или Товар
End Class

Public Class Chainmaster

    Property ChID As String  ' ID сети в БД CISLink, первичный ключ
    Property ChOid As String
    Property ChainName As String  ' Имя сети на английском
    Property ChainNameRuss As String  ' Имя сети на русском
    Property ChainType As String     ' Тип сети
    Property ChainTypeID As String   ' Код типа сети
    Property Chainge As Boolean

End Class

Public Class DistrSAPCode

    Property Oid As String
    Property DistOid As String
    Property CodeOLD As String
    Property CodeNEW As String
    Property Y As Integer
    Property M As Integer

    ReadOnly Property DateCode As Date
        Get
            Return New Date(Y, M, 1)
        End Get
    End Property

End Class

Public Class NPRAforSAP

    Property KOZGF As String
    Property KOLNR As String
    Property KOTABNR As String
    Property KSCHL As String
    Property MODE As String
    Property SPLIT_IND As String
    Property BOART As String
    Property VKORG As String
    Property VTWEG As String
    Property SPART As String
    Property BOTEXT As String
    Property BONEM As String
    Property WAERS As String
    Property ZLSCH As String
    Property ABREX As String
    Property DATAB As Date
    Property DATBI As Date
    Property KUNNR As String
    Property ZZPRODH2 As Integer
    Property KONP_KBETR As Double
    Property KONP_KBRUE As Double
    Property BOMAT As String
    Property KRECH As String

End Class